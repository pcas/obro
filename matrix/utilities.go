// Utilities contains utility functions used by the Obro code.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package matrix

import (
	"bitbucket.org/pcasmath/integer"
	"bitbucket.org/pcasmath/integervector"
)

/////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////

// VectorFromIntSlice returns an integer vector in the lattice L from the given slice.  It will panic if the dimension of L does not match the length of S.
func VectorFromIntSlice(L *integervector.Parent, S []int) *integervector.Element {
	v, err := integervector.FromIntSlice(L, S)
	if err != nil {
		panic(err)
	}
	return v
}

// VectorFromIntegerSlice returns an integer vector in the lattice L from the given slice.  It will panic if the dimension of L does not match the length of S.
func VectorFromIntegerSlice(L *integervector.Parent, S []*integer.Element) *integervector.Element {
	v, err := integervector.FromIntegerSlice(L, S)
	if err != nil {
		panic(err)
	}
	return v
}

// VectorSum returns the sum of the given vectors.
func VectorSum(S ...*integervector.Element) *integervector.Element {
	if len(S) == 0 {
		panic("can't take the sum of a zero length sequence")
	}
	L, err := integervector.DefaultLattice(S[0].Dimension())
	if err != nil {
		panic(err)
	}
	v, err := integervector.Sum(L, S...)
	if err != nil {
		panic(err)
	}
	return v
}

// dotProduct returns the dot-product of the two vectors. This will panic if the dimensions differ.
func DotProduct(x *integervector.Element, y *integervector.Element) *integer.Element {
	c, err := integervector.DotProduct(x, y)
	if err != nil {
		panic(err)
	}
	return c
}
