// Matrix is a very simple matrix implementation with the minimum amount of functionality needed for the Obro code.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package matrix

import (
	"bitbucket.org/pcasmath/integer"
	"bitbucket.org/pcasmath/integervector"
	"strings"
)

// Matrix represents a matrix with integer entries, given by a slice of vectors for the columns.
type Matrix []*integervector.Element

// EntryOrPanic returns the (i,j) entry of a matrix.  i and j both index from 0. This will panic if either i or j is out of range.
func (m Matrix) EntryOrPanic(i int, j int) *integer.Element {
	return m[j].EntryOrPanic(i)
}

// Column returns the j-th column of a matrix.  j indexes from 0
func (m Matrix) Column(j int) *integervector.Element {
	return m[j]
}

// Columns returns the columns of a matrix, as a slice of integer vectors
func (m Matrix) Columns() []*integervector.Element {
	return integervector.CopySlice(m)
}

// NumberOfRows returns the number of rows of the matrix.
func (m Matrix) NumberOfRows() int {
	if len(m) == 0 {
		panic("can't take the number of rows of an empty matrix")
	}
	return m[0].Dimension()
}

// NumberOfColumns returns the number of columns of the matrix.
func (m Matrix) NumberOfColumns() int {
	return len(m)
}

// RowSums returns a slice S of integers, where the i-th entry of S is the sum of the i-th row of the matrix m.
func (m Matrix) RowSums() []*integer.Element {
	if len(m) == 0 {
		return make([]*integer.Element, 0)
	}
	M := m[0].Parent().(*integervector.Parent)
	v, err := integervector.Sum(M, m...)
	if err != nil {
		panic(err)
	}
	return v.ToIntegerSlice()
}

// FromColumns returns a new matrix with columns given by columns, which is a slice of integer vectors
func (m Matrix) FromColumns(columns []*integervector.Element) Matrix {
	return Matrix(columns)
}

// String returns a string representation of the matrix.
func (m Matrix) String() string {
	if len(m) == 0 {
		return "(())"
	}
	result := "(\n"
	numRows := m[0].Dimension()
	numCols := len(m)
	for i := 0; i < numRows; i++ {
		coeffs := make([]string, 0, len(m))
		for j := 0; j < numCols; j++ {
			coeffs = append(coeffs, m.EntryOrPanic(i, j).String())
		}
		result += "(" + strings.Join(coeffs, ",") + ")\n"
	}
	return result + ")"
}

// IdentityMatrix returns the dim x dim identity matrix
func IdentityMatrix(dim int) Matrix {
	var result Matrix
	var zZn *integervector.Parent
	var err error
	if zZn, err = integervector.DefaultLattice(dim); err != nil {
		panic(err)
	}
	columns := make([]*integervector.Element, dim)
	col := make([]int, dim)
	for i := 0; i < dim; i++ {
		for j := 0; j < dim; j++ {
			if j == i {
				col[j] = 1
			} else {
				col[j] = 0
			}
		}
		columns[i] = VectorFromIntSlice(zZn, col)
	}
	return result.FromColumns(columns)
}

// VectorMultiplyMatrix returns the product v*m   (i.e. the matrix multiplies the row vector v on the right)
func VectorMultiplyMatrix(v *integervector.Element, m Matrix) *integervector.Element {
	if m.NumberOfRows() != v.Dimension() {
		panic("mismatch of dimensions in vectorMultiplyMatrix")
	} else if m.NumberOfColumns() == 0 {
		panic("Empty matrix in vectorMultiplyMatrix")
	} else if v.Dimension() == 0 {
		panic("Empty vector in vectorMultiplyMatrix")
	}
	xs := make([]*integer.Element, m.NumberOfColumns())
	for j := 0; j < m.NumberOfColumns(); j++ {
		xs[j] = DotProduct(v, m.Column(j))
	}
	var zZn *integervector.Parent
	var err error
	if zZn, err = integervector.DefaultLattice(len(xs)); err != nil {
		panic(err)
	}
	return VectorFromIntegerSlice(zZn, xs)
}
