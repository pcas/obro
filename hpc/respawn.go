// Respawn defines a function to respawn a process on the Imperial HPC cluster

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package hpc

import (
	"bitbucket.org/pcastools/log"
	"bytes"
	"os/exec"
)

// Spawn runs command as a shell command.  (This should submit an obro job to the HPC scheduler.) It writes logging messages to lg.
func Spawn(lg log.Interface, command string) {
	lg = log.PrefixWith(lg, "[Spawn]")
	lg.Printf("Command is %s", command)
	respawn := exec.Command("bash", "-c", command)
	var stdout, stderr bytes.Buffer
	respawn.Stdout = &stdout
	respawn.Stderr = &stderr
	lg.Printf("Starting respawn command")
	err := respawn.Start()
	if err != nil {
		lg.Printf("Error executing respawn command: %v", err)
	}
	lg.Printf("Waiting for respawn command to finish")
	err = respawn.Wait()
	if err != nil {
		lg.Printf("respawn command finished with error: %v", err)
		lg.Printf("stdout: %s", stdout.String())
		lg.Printf("stderr: %s", stderr.String())
	} else {
		lg.Printf("respawn command finished successfully")
	}
	return
}
