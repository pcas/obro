#!/bin/bash
#PBS -l select=1:ncpus=24:mem=24gb
#PBS -N obro
#PBS -q pqfano
#PBS -l walltime=01:00:00
#PBS -e /home/tomc/qsublogs/obro
#PBS -o /home/tomc/qsublogs/obro

export PCAS_FS_ADDRESS="fano.ma.ic.ac.uk:12358"
export PCAS_JOB_ADDRESS="fano.ma.ic.ac.uk:12360"
export PCAS_KVDB_ADDRESS="fano.ma.ic.ac.uk:12356"
export PCAS_LOCK_ADDRESS="fano.ma.ic.ac.uk:12351"
export PCAS_LOG_ADDRESS="fano.ma.ic.ac.uk:12354"
export PCAS_METRICS_ADDRESS="fano.ma.ic.ac.uk:12355"
export PCAS_MONITOR_ADDRESS="fano.ma.ic.ac.uk:12357"
export PCAS_RANGEDB_ADDRESS="fano.ma.ic.ac.uk:12357"

export PCAS_SSL_CERT="$(cat $HOME/pcas/machines/cert/fano.ma.ic.ac.uk.crt)"

export GOMAXPROCS=24
DIM=7
RUNDURATION=5m
DIR="${HOME}/tt"

exec "${DIR}/obro" -dimension=${DIM} -log-to-logd run -run-duration=${RUNDURATION} -respawn-command="qsub /home/tomc/tt/obro.sh"
