module bitbucket.org/pcas/obro

go 1.16

require (
	bitbucket.org/pcas/hpc v0.1.2
	bitbucket.org/pcas/keyvalue v0.1.24
	bitbucket.org/pcas/logger v0.1.27
	bitbucket.org/pcas/metrics v0.1.22
	bitbucket.org/pcas/sslflag v0.0.11
	bitbucket.org/pcasmath/integer v0.0.1
	bitbucket.org/pcasmath/integervector v0.0.1
	bitbucket.org/pcastools/cache v1.0.2
	bitbucket.org/pcastools/cleanup v1.0.3
	bitbucket.org/pcastools/fatal v1.0.2
	bitbucket.org/pcastools/flag v0.0.16
	bitbucket.org/pcastools/gobutil v1.0.3
	bitbucket.org/pcastools/log v1.0.3
	bitbucket.org/pcastools/rand v1.0.3 // indirect
	bitbucket.org/pcastools/slice v1.0.3
	bitbucket.org/pcastools/stringsbuilder v1.0.2
	bitbucket.org/pcastools/ulid v0.1.3
	bitbucket.org/pcastools/version v0.0.4
	github.com/klauspost/compress v1.12.1 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc // indirect
	golang.org/x/net v0.0.0-20210415231046-e915ea6b2b7d // indirect
	golang.org/x/sys v0.0.0-20210415045647-66c3f260301c // indirect
	google.golang.org/genproto v0.0.0-20210416161957-9910b6c460de // indirect
)
