// Config.go defines global command-line flags and environment variables for obro.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	"bitbucket.org/pcas/keyvalue/kvdb/kvdbflag"
	"bitbucket.org/pcas/logger/logd/logdflag"
	"bitbucket.org/pcas/metrics/metricsdb/metricsdbflag"
	"bitbucket.org/pcas/obro/obrocmd"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
)

// Options describes the options.
type Options struct {
	Run       obrocmd.RunFunc // The run function for the command
	Dimension int             // The dimension of Fano polytopes to classify
	SSLCert   []byte          // The SSL certificate (if any)
}

// Name is the name of the executable.
const Name = "obro"

// DatabaseName is the name of the database in which results and intermediate computations are stored.
const DatabaseName = "Obro"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts, err := defaultOptions()
	assertNoErr(err)
	// Parse and validate the configuration information
	assertNoErr(parseArgs(opts))
	return opts
}

// assertNoErr halts execution if the given error is non-nill. If the error is non-nil then it will be printed to os.Stderr, and then os.Exit will be called with a non-zero exit code.
func assertNoErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", Name, err)
		os.Exit(1)
	}
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() (*Options, error) {
	return &Options{
		Dimension: 1,
	}, nil
}

// validate validates the options.
func validate(opts *Options) error {
	if opts.Dimension <= 0 {
		return fmt.Errorf("the dimension must be positive")
	} else if opts.Run == nil {
		return fmt.Errorf("no command specified")
	}
	return nil
}

// commandFlagSet returns a dummy flag set, with the empty set of flags and a usage headers and footer that describe the commands
func commandFlagSet() flag.Set {
	cmds := flag.NewBasicSet("Commands")
	var usage strings.Builder
	w := tabwriter.NewWriter(&usage, 12, 1, 2, ' ', 0)
	for i, c := range obrocmd.Cmds() {
		if i != 0 {
			fmt.Fprintf(w, "\n")
		}
		fmt.Fprintf(w, "  %s\t%s", c.String(), c.Description())
	}
	w.Flush()
	cmds.SetUsageHeader(usage.String())
	cmds.SetUsageFooter(fmt.Sprintf("Help for a specified command can be obtained via:\n  %s [cmd] -h", Name))
	return cmds
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) error {
	// Create the standard SSL flag
	sslFlag := &sslflag.Flag{}
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf(`%s is a distributed implementation of Øbro's algorithm for classifying Fano polytopes.  It uses the pcas key-value database kvdb to store the results and intermediate computations, and optionally reports metrics to the pcas metrics server.
	
	Usage: %s [flags] [cmd [cmd opts] [cmd args]]`, Name, Name))
	flag.SetName("Options")
	flag.Add(
		flag.Int("dimension", &opts.Dimension, opts.Dimension, "The dimension of smooth Fano polytopes to classify", ""),
		sslFlag,
		&version.Flag{AppName: Name},
	)
	// Add a dummy flag set that provides a description of the commands
	flag.AddSet(commandFlagSet())
	// Create and add the kvdb flag set
	kvdbSet := kvdbflag.NewSet(nil)
	flag.AddSet(kvdbSet)
	// Create and add the logd flag set
	logdSet := logdflag.NewLogSet(nil)
	flag.AddSet(logdSet)
	// Create and add the metrics flag set
	metricsdbSet := metricsdbflag.NewSet(nil)
	flag.AddSet(metricsdbSet)
	// Parse the flags
	flag.Parse()
	// Recover the SSL certificate
	var err error
	if opts.SSLCert, err = sslFlag.Certificate(); err != nil {
		return err
	}
	// Check that a command is specified
	args := flag.Args()
	if len(args) == 0 {
		return errors.New("no command specified")
	}
	cmd, ok := obrocmd.Get(args[0])
	if !ok {
		return errors.New("unknown command: " + args[0])
	}
	// Parse the command
	if opts.Run, err = cmd.Parse(args[1:]); err != nil {
		return err
	}
	// Validate the options
	if err := validate(opts); err != nil {
		return err
	}
	// Set the kvdb configuration
	kvdbSet.SetDefault(Name, opts.SSLCert)
	// Set the metrics endpoint
	metricsdbSet.SetMetrics(context.Background(), Name, opts.SSLCert)
	// Set the loggers
	return logdSet.SetLogger(context.Background(), Name, opts.SSLCert)
}
