// Services defines an object representing the pcas logging and key:value services

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package services

/*

TODO remove

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/netutil"
	"context"
	"github.com/pkg/errors"
	"sync"
)

// Services represents a logger (which might log to stderr or to the pcas logging server), a metrics endpoint, and a connection to the pcas kvdbd server
type Services struct {
	lg  log.Interface     // The logger
	met metrics.Interface // The metrics endpoint
	c   connection        // The database connection
}

// connection is used to maintain a connection to the database. This implements the Connection interface.
type connection struct {
	log.BasicLogable
	m        sync.Mutex           // Controls access to the following
	conn     *keyvalue.Connection // The underlying database connection
	isClosed bool                 // Are we closed?
	closeErr error                // The error on close (if any)
}

// Connection is a subset of the *keyvalue.Connection methods.
type Connection interface {
	log.Logable
	DriverName() string                                  // DriverName returns the name of the associated driver.
	DataSource() string                                  // DataSource returns the data source string used to establish the connection. Note that this may contain sensitive user data such as a password.
	ConnectToTable(name string) (*keyvalue.Table, error) // ConnectToTable connects to the indicated table in the database.
}

// dataSource is the data source name for our database.
const dataSource = "mongodb://Obro"

//////////////////////////////////////////////////////////////////////
// Services functions
//////////////////////////////////////////////////////////////////////

// New returns a new *Services object with the specified logger, metrics endpoint, and kvdb connection with the specified server name and port.  It is the caller's responsibility to shut down these services before exiting.
func New(lg log.Interface, met metrics.Interface, server string, port int) *Services {
	// Set the kvdbd connection details and start the kvdbd client
	kvdb.SetLogger(lg)
	kvdb.Start(netutil.DefaultTCPDialWithContextFunc(server, port, lg))
	kvdb.SetLogger(log.Discard)
	// Create the services and set the logs
	S := &Services{
		lg:  lg,
		met: met,
	}
	// turn off connection-level logging
	S.Connection().SetLogger(log.Discard)
	return S
}

// Close closes the connection to the services.
func (S *Services) Close() (err error) {
	// Sanity check
	if S == nil {
		return nil
	}
	// Close the database connection
	if e := (&S.c).closeConnection(); e != nil {
		err = errors.Wrap(e, "Error closing database connection")
	}
	// Stop the kvdbd client
	if e := kvdb.Stop(); e != nil && err == nil {
		err = errors.Wrap(e, "Error stopping kvdbd client")
	}
	// We don't need to close the logger, as this is handled by cleanup.Run()
	return
}

// Log returns the log.
func (S *Services) Log() log.Interface {
	if S == nil {
		return log.Discard
	}
	return S.lg
}

// Metrics returns the metrics endpoint.
func (S *Services) Metrics() metrics.Interface {
	if S == nil {
		return metrics.Discard
	}
	return S.met
}

// Connection returns the database connection.
func (S *Services) Connection() Connection {
	if S == nil {
		return &connection{isClosed: true}
	}
	return &S.c
}

//////////////////////////////////////////////////////////////////////
// Connection functions
//////////////////////////////////////////////////////////////////////

// closeConnection closes the underlying database connection, and prevents new connections from being established.
func (c *connection) closeConnection() error {
	c.m.Lock()
	if !c.isClosed {
		c.isClosed = true
		if c.conn != nil {
			c.closeErr = c.conn.Close()
			c.conn = nil
		}
	}
	err := c.closeErr
	c.m.Unlock()
	return err
}

// DriverName returns the name of the associated driver.
func (*connection) DriverName() string {
	return kvdb.DriverName
}

// DataSource returns the data source string used to establish the connection. Note that this may contain sensitive user data such as a password.
func (c *connection) DataSource() string {
	return dataSource
}

// ConnectToTable connects to the indicated table in the database.
func (c *connection) ConnectToTable(name string) (*keyvalue.Table, error) {
	// Acquire a lock and defer unlocking
	c.m.Lock()
	defer c.m.Unlock()
	// Is there anything to do?
	if c.isClosed {
		return nil, errors.New("The connection is closed")
	}
	// We make at most maxConnectionAttempts attempt to connect to the table
	const maxConnectionAttempts = 3
	var attemptErr error
	for attempt := 0; attempt < maxConnectionAttempts; attempt++ {
		// Do we own a connection to the database? If not, establish one
		if c.conn == nil {
			conn, err := keyvalue.Open(context.Background(), c.DriverName(), c.DataSource())
			if err != nil {
				// Unable to connect to the database
				return nil, err
			}
			conn.SetLogger(c.Log())
			c.conn = conn
		}
		// Connect to the table
		t, err := c.conn.ConnectToTable(name)
		if err == nil {
			// Success
			return t, nil
		}
		// If we're here the connection failed -- close the underlying connection
		// ready for a fresh attempt
		c.conn.Close()
		c.conn = nil
		attemptErr = err
	}
	return nil, attemptErr
}

*/
