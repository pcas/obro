// Obrocmd implements the subcommands "obro run", "obro initialise", etc.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package obrocmd

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"sort"
)

// Cmd represents a command.
type Cmd struct {
	name  string
	desc  string
	parse parseFunc
}

// RunFunc is a function that can be run to execute a subcommand.
type RunFunc func(dim int, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) error

// parseFunc is a parse function for a command. The command-line arguments will be passed in 'args'. The parse function should return a run function.
type parseFunc func(args []string) (RunFunc, error)

// registry contains the registered commands.
var registry map[string]*Cmd

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// register registers the a new command with given name, description, and parse function. This will panic if the name is already in use.
func register(name string, desc string, parse parseFunc) {
	if registry == nil {
		registry = make(map[string]*Cmd)
	}
	if _, ok := registry[name]; ok {
		panic("Command already registered with name " + name)
	}
	registry[name] = &Cmd{
		name:  name,
		desc:  desc,
		parse: parse,
	}
}

/////////////////////////////////////////////////////////////////////////
// Cmd functions
/////////////////////////////////////////////////////////////////////////

// String returns the command name.
func (c *Cmd) String() string {
	if c == nil {
		return ""
	}
	return c.name
}

// Description returns a brief one-line description of the command.
func (c *Cmd) Description() string {
	if c == nil {
		return ""
	}
	return c.desc
}

// Parse attempts to generate a run function from the given command-line arguments.
func (c *Cmd) Parse(args []string) (RunFunc, error) {
	// Sanity check
	if c == nil || c.parse == nil {
		return nil, nil
	}
	// Create the run function
	return c.parse(args)

}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Get returns the command with the given name, if any.
func Get(name string) (*Cmd, bool) {
	c, ok := registry[name]
	return c, ok
}

// Cmds returns a slice of all available commands, sorted in increasing order by command name.
func Cmds() []*Cmd {
	// Create the slice of available commands
	cmds := make([]*Cmd, 0, len(registry))
	for _, c := range registry {
		cmds = append(cmds, c)
	}
	// Sort the slice and return
	sort.Slice(cmds, func(i int, j int) bool {
		return cmds[i].String() < cmds[j].String()
	})
	return cmds
}
