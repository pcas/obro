// Status implements the "status" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package obrocmd

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/status"
	"bitbucket.org/pcastools/log"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("status", "Gets status information for the search in the given dimension.", parseStatus)
}

// parseStatus parses the given arguments for the command, returning a run function on success.
func parseStatus(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("status", flag.ExitOnError)
	f.Usage = usageStatus
	// Parse the (nonexistent) flags
	f.Parse(args)
	// Return the run function
	return createStatusFunc(), nil
}

// usageStatus writes a usage message describing the arguments for the command.
func usageStatus() {
	fmt.Fprint(os.Stderr, `Command "status" gets status information for the search in the given dimension.

Command usage: status
`)
}

// createStatusFunc returns a run function to get status for the search in dimension dim.
func createStatusFunc() RunFunc {
	return func(dim int, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) (err error) {
		return status.Status(dim, conn, met, lg)
	}
}
