// Release implements the "release" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package obrocmd

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/release"
	"bitbucket.org/pcastools/log"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("release", "Clears certain cached data in the given dimension.", parseRelease)
}

// parseRelease parses the given arguments for the command, returning a run function on success.
func parseRelease(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("release", flag.ExitOnError)
	f.Usage = usageRelease
	// Parse the (nonexistent) flags
	f.Parse(args)
	// Return the run function
	return createReleaseFunc(), nil
}

// usageRelease writes a usage message describing the arguments for the command.
func usageRelease() {
	fmt.Fprint(os.Stderr, `Command "release" clears the cache of partial polytopes, in the given dimension,
that are marked as being processed.

Command usage: release
`)
}

// createReleaseFunc returns a run function to release the polytopes, in dimension dim, that are marked as being processed.
func createReleaseFunc() RunFunc {
	return func(dim int, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) (err error) {
		return release.Release(dim, conn, met, lg)
	}
}
