// Initialise implements the "initialise" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package obrocmd

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/initialise"
	"bitbucket.org/pcastools/log"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("initialise", "Initialise the search in the given dimension.", parseInitialise)
}

// parseInitialise parses the given arguments for the command, returning a run function on success.
func parseInitialise(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("initialise", flag.ExitOnError)
	f.Usage = usageInitialise
	// Parse the (nonexistent) flags
	f.Parse(args)
	// Return the run function
	return createInitialiseFunc(), nil
}

// usageInitialise writes a usage message describing the arguments for the command.
func usageInitialise() {
	fmt.Fprint(os.Stderr, `Command "initialise" initialises the search in the given dimension.  This
deletes all currently stored data in that dimension.

Command usage: initialise
`)
}

// createInitialiseFunc returns a run function to initialise the search in dimension dim.
func createInitialiseFunc() RunFunc {
	return func(dim int, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) (err error) {
		return initialise.Initialise(dim, conn, met, lg)
	}
}
