// Run implements the "run" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package obrocmd

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/run"
	"bitbucket.org/pcastools/log"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
	"time"
)

// runOptions describes the options for the run command.
type runOptions struct {
	RespawnCommand string        // The command (if any) used to respawn at the end of a run
	RunDuration    time.Duration // The length of time to run for
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("run", "Runs the search in the given dimension.", parseRun)
}

// parseRun parses the given arguments for the command, returning a run function on success.
func parseRun(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("run", flag.ExitOnError)
	opts := &runOptions{}
	f.Usage = func() { usageRun(opts) }
	// Set the flags
	f.StringVar(&opts.RespawnCommand, "respawn-command", opts.RespawnCommand, "The command (if any) to respawn at the end of the run.")
	f.DurationVar(&opts.RunDuration, "run-duration", opts.RunDuration, "The length of time to run for.")
	// Parse the flags
	f.Parse(args)
	// Build and return the run function
	return createRunFunc(opts), nil
}

// usageRun writes a usage message describing the arguments for the command.
func usageRun(opts *runOptions) {
	fmt.Fprint(os.Stderr, `Command "run" runs the search in the given dimension.

Command usage: run [flags]

Optional flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprintf(w, "  -respawn-command\tThe command (if any) to respawn at the end of the run\n\t(default: \"%s\").\n", opts.RespawnCommand)
	fmt.Fprintf(w, "  -run-duration\tThe length of time to run for (default: %v).\n", opts.RunDuration)
	w.Flush()
}

// createRunFunc returns a run function to run the search in dimension dim.  Configuration data for this run function are taken from opts.
func createRunFunc(opts *runOptions) RunFunc {
	return func(dim int, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) (err error) {
		return run.Run(dim, opts.RespawnCommand, opts.RunDuration, conn, met, lg)
	}
}
