// To compile:  g++ -xc++ Obro.cpp -o Obro

#include<fstream> 
#include<vector> 
#include<string> 
#include<iostream> 
#include<sstream> 
#include<numeric>

using namespace std; 

typedef vector < vector<int> > matrix; 

// The dimension d 
int d; 

// Global variables for facets 
matrix facets; 
matrix normals; 
vector < matrix > basechanges; 
matrix neighboring_facets; 

// Global variables for vertices 
matrix vertices; 
vector < int > neighboring_vertices; 
vector < int > neighbors_in_hyperplane; 

// Global constants 
matrix zero_matrix; 
vector < int > minus_vector; 
vector < int > zero_vector;

// Output to files 
vector < int > number_of_equivalence_classes; 
ofstream fanovert; 
ofstream msg; 

// PRINTING FUNCTIONS 
void print_vector(vector < int > v) 
{ 
	for(int i=0;i<v.size();++i) 
		msg << v[i] << " "; 
} 

// OUTPUT TO FILE 
void output_vertices() 
{ 
	for(int i=0;i<vertices.size();++i) 
	{ 
		for(int j=0;j!=d;++j) 
			fanovert << vertices[i][j] << " "; 
		fanovert << "\n"; 
	} 
	fanovert << endl; 
	++number_of_equivalence_classes[vertices.size()]; 
} 

// LINEAR ALGEBRA FUNCTIONS 
inline int dotprod ( vector <int> &v , vector <int> &w ) 
{ 
	int result=0; 
	for(int i=0;i!=d;++i) 
		result+=v[i]*w[i]; 
	return result; 
} 

inline int vect_mult_col ( vector < int > &v , matrix &A , int col ) 
{ 
	int res=0; 
	for(int i=0;i<d;++i) 
		res+=v[i]*A[i][col]; 
	return res; 
} 

inline void vect_mult_matr ( vector < int > &v , matrix &A , vector < int > &result ) 
{
	result.resize(d); 
	for(int j=0;j<d;++j) 
		result[j]=vect_mult_col(v,A,j); 
} 

inline void matr_mult_matr ( matrix &A , matrix &B , matrix &C ) 
{ 
	C.resize(A.size(),zero_vector); 
	for(int i=0;i!=A.size();++i) 
		vect_mult_matr(A[i],B,C[i]); 
} 

// Add the rows of matrix 
inline void add_rows ( const matrix &M , vector < int > &sum ) 
{ 
	sum.resize(d,0); 
	for(int col=0;col<d;++col) 
		for(int row=0;row<M.size();++row) 
			sum[col]+=M[row][col]; 
} 

// Add the entries of a vector 
inline int add_entries ( vector < int > &v ) 
{ 
	return accumulate(v.begin(),v.end(),0); 
} 

/* 
vertices have numbers (ints)
facets have numbers too (ints)
each facet is simplicial, so it contains d vertices, and facets[facet_idx] is the length-d vector consisting of the indices of the vertices in the facet with index facet_idx
ridges are intersections of facets, that is, they are codimension-2 faces.  Each necessarily has d-1 vertices.  (because every face is a simplex!)
ridges are represented by length-(d-1) vectors consisting of the indices of the vertices that the ridge contains

 */
// FACET ADDING FUNCTIONS 
// Is ridge in facet?
// returns -1 if ridge is not in the facet (which it detects by checking if there are two vertices in the ridge that are not in the facet)
// if the ridge is in the facet, return the index of the vertex in facet that is not in ridge
inline int ridge_in_facet ( int facet , vector < int > &ridge ) 
{ 
	int opposite=-1; 
	for(int i=0;i<d;++i) 
	{ 
		if(find(ridge.begin(),ridge.end(),facets[facet][i])==ridge.end()) 
		{ 
			// Return -1 if ridge not in facet 
			if(opposite!=-1) 
				return -1; 
			opposite=i; 
		} 
	} 
	// Return placement of opposite in facet_vertex_vector
	return opposite; 
} 

/* neighboring_facets[facet_idx] is a vector of length d, the entries of which are ints and should be thought of as vertex ids.  Let F be the facet indexed by facet_id and let v be the vertex indexed by vertex_id.  Let R be the ridge given by removing v from F. if R is the ridge of some other facet with id n then neighboring_facets[facet_idx][vertex_id] is equal to n; otherwise it is equal to -1.     NOTE: ultimately there should be precisely one other facet attached to every ridge of F but during the construction we may not have constructed the neighbouring facet yet.  It is also possible that the most recently added facet is bogus in the sense that a "ridge" connects to more than two facets; this function will detect that by returning false.  Otherwise it will update the neighbouring facet information and return true.
 */
// Close ridges of a facet just added 
inline bool close_ridges () 
{ 
	for(int i=0;i<d;++i) 
	{ 
		// Create ridge 
		vector < int > ridge; 
		for(int j=0;j<d;++j) 
			if(j!=i) 
				ridge.push_back((facets.back())[j]); 
		/* the ridge is now the last facet with its i-th vertex omitted */
		// Find a facet containing this ridge 
		// (not the one just added)
		for(int facet=0;facet<facets.size()-1;++facet) 
		{ 
			int placement=ridge_in_facet(facet,ridge); 
			if(placement!=-1) 
			{ 
				// Return false, if neighboring facet not unique 
				if(neighboring_facets[facet][placement]!=-1) 
					return false; 
				// Join facets and close ridge 
				neighboring_facets[facet][placement]=facets.size()-1; 
				(neighboring_facets.back())[i]=facet; 
				break; 
			} 
		} 
	} 
	return true; 
} 

bool vertex_facet_check ( int facet , int vertex_number ); 

/* 
   Let F be the existing facet indexed by oldfacet.  
   Let v be the new vertex that we are going to add in, indexed by neighbouring_vertex.
   Let R be the ridge of F determined by the vertex of F with index opposite_number.
   Then the new facet F' is the convex hull of R union {v}; we update the associated data:
   normals -- adding the outward-pointing normal to F' to the end of this sequence
   neighbouring_facets -- where we first add (-1,-1,...,-1) to the end of that sequence and then call close_ridges to update it
   check if any of the existing vertices lie on the wrong side of the supporting hyperplane

   this function returns false if there are any contradiction from neighbouring_facets or the hyperplane checks, and true otherwise
*/
bool make_new_facet ( int oldfacet , int neighboring_vertex , int opposite_number ) 
{ 
	// Add new facet 
  vector <int> new_facet(facets[oldfacet]);   // make F'
  new_facet[opposite_number]=neighboring_vertex; 
  facets.push_back(new_facet);  // add F' to the list of facets
  // New basechange matrix     /* IGNORE THIS -- we are just updating the list of normal vectors */
  vector < int > relation; 
  vect_mult_matr(vertices[neighboring_vertex],basechanges[oldfacet],relation); 
  relation[opposite_number]=-2; 
  basechanges.push_back(basechanges[oldfacet]); 
  for(int i=0;i<d;++i) 
    for(int j=0;j<d;++j) 
      (basechanges.back())[i][j]+=basechanges[oldfacet][i][opposite_number]*relation[j]; 
  // New normal 
  vector < int > new_normal(d,0); 
  for(int i=0;i<d;++i) 
    new_normal[i]=add_entries((basechanges.back())[i]); 
  normals.push_back(new_normal); 
  // New neighbors 
  neighboring_facets.push_back(minus_vector); 
  // Close ridges 
  if(close_ridges()==false) 
    return false; 
  // Consequences of new facet 
  int f_end=facets.size()-1; 
  for(int v=0;v!=vertices.size();++v) 
    if(vertex_facet_check(f_end,v)==false) 
			return false; 
  return true; 
} 

/* normals[i] dots to 1 on all vertices in the i-th facet and dots to <=0 on all other vertices.  In other words, these are
   outward-pointing normals

   THINK: this appears to look for a cheap contradiction to throwing in vertex
*/
bool vertex_facet_check ( int facet , int vertex ) 
{ 
  // If vertex is in facet , then no need to check 
  if(find(facets[facet].begin(),facets[facet].end(),vertex)==facets[facet].end()) 
    { 
      // In which hyperplane is vertex?
      int hyperplane=dotprod(normals[facet],vertices[vertex]); 
      // If vertex is not strictly beneath facet, return false 
      if(hyperplane>0) 
	return false; 
      for(int j=0;j<d;++j) 
	{ 
	  if(neighboring_facets[facet][j]==-1) 
	    { 
	      // Calculate coeficient to basis vector j 
	      // with respect to basis V(facet) 
	      int coef=vect_mult_col(vertices[vertex],basechanges[facet],j); 
	      // Add a facet , if vertex is in zero-hyperplane 
	      // or a neighboring vertex of initial facet 
	      if(hyperplane==0 || facet==0) 
		{ 
		  if(coef<-1 || (coef==-1 && make_new_facet(facet,vertex,j)==false)) 
		    return false; 
		} 
	      else 
		// Return false, if coef exceeds lower bound
		/* THINK we don't understand where this bound comes from yet, but it looks reasonable */
		if(coef<hyperplane) 
		  return false; 
	    } 
	} 
    } 
  return true; 
} 

// Could the vertex set be a presubset of a smooth Fano d-polytope?
/* I've added the last vertex, provisionally.  Did this cause any problems?  Problems here means vertex_facet_check failing */
bool checksubset () 
{ 
	// Is new vertex beneath every known facet? 
	int f_end=facets.size(); 
	for(int facet=1;facet<f_end;++facet) 
		if(dotprod(normals[facet],vertices.back())>0) 
			return false;
	// Consequences of new vertex 
	for(int facet=0;facet<f_end;++facet) 
		if(vertex_facet_check(facet,vertices.size()-1)==false) 
			return false; 
	return true; 
} 

bool add_neighboring_facet ( int facet , int opposite_number ) 
{ 
	// Find hyperplane and coeficient for each vertex 
	vector < int > h; 
	vector < int > c; 
	for(int v=0;v<vertices.size();++v) 
	{ 
		h.push_back(dotprod(vertices[v],normals[facet])); 
		c.push_back(vect_mult_col(vertices[v],basechanges[facet],opposite_number)); 
	} 
	// Find neighboring vertex 
	int neighboring_vertex=-1; 
	for(int v=0;v<vertices.size();++v) 
	{ 
		if(c[v]==-1) 
		{ 
			if(neighboring_vertex==-1 || h[neighboring_vertex]<h[v]) 
				neighboring_vertex=v; 
		} 
	} 
	// If no neighboring vertex, return false 
	if(neighboring_vertex==-1) 
		return false; 
	// If another vertex is not beneath the coming 
	// neighboring facet, then return false 
	for(int v=0;v<vertices.size();++v) 
	{ 
		if(neighboring_vertex!=v && c[v]<0) 
		{ 
			if(h[v]+c[v]*(h[neighboring_vertex]-1)>0) 
				return false; 
		}
	} 
	// Create new facet 
	return make_new_facet(facet,neighboring_vertex,opposite_number); 
} 

bool conv_is_fano () 
{ 
	// For every facet: Try to add missing neighboring facets 
	int f=0; 
	while(facets.size()>f) 
	{ 
		for(int j=0;j<d;++j) 
			if(neighboring_facets[f][j]==-1 && add_neighboring_facet(f,j)==false) 
				return false; 
		++f; 
	} 
	// Every facet had neighboring facets 
	return true; 
} 

// UNDO ADDED FACETS 
void remove_facets( int new_size ) 
{ 
	if(new_size!=facets.size()) 
	{ 
		// Erase some facets 
		facets.resize(new_size); 
		normals.resize(new_size); 
		basechanges.resize(new_size); 
		neighboring_facets.resize(new_size); 
		// Open ridges 
		for(int f=0;f!=new_size;++f) 
		{ 
			for(int j=0;j<d;++j) 
				if(neighboring_facets[f][j]>=new_size) 
					neighboring_facets[f][j]=-1; 
		} 
	} 
}

// COMPARISON FUNCTIONS 
// All function returns 
// -1 if first input smallest 
// 1 if second input smallest 
// 0 if equal input 
inline int compare_points ( vector < int > &v1 , vector < int > &v2 ) 
{ 
	int h1=accumulate(v1.begin(),v1.end(),0); 
	int h2=accumulate(v2.begin(),v2.end(),0); 
	if(h1>h2) 
		return -1; 
	if(h1<h2) 
		return 1; 
	for(int i=0;i<d;++i) 
	{ 
		if(v1[i]<v2[i]) 
			return -1; 
		if(v1[i]>v2[i]) 
			return 1; 
	} 
	return 0; 
} 

// Swaps two columns of the matrix from a given row to the bottom 
inline void swap_columns ( matrix &M , int column1 , int column2 , int row ) 
{ 
	int x; 
	for(int v=row;v<M.size();++v) 
	{ 
		x=M[v][column1]; 
		M[v][column1]=M[v][column2]; 
		M[v][column2]=x; 
	} 
} 

// If two columns are equal from a given row to the top, return true 
// Otherwise return false 
inline bool equal_column_vectors ( matrix &M , int column1 , int column2 , int row ) 
{ 
	for(int i=0;i<row;++i) 
		if(M[i][column1]!=M[i][column2]) 
			return false; 
	return true;
} 

// Minimizes a row (with respect to lex order) fixing the matrix 
// from a given row to the top 
void minimize_row ( matrix &M , int row ) 
{ 
	for(int column1=0;column1<d;++column1) 
	{ 
		// Find a column to swap with, to obtain a smaller row vector 
		int column2=column1; 
		for(int i=column1+1;i<d;++i) 
			if(M[row][i]<M[row][column2] && equal_column_vectors(M,column1,i,row)) 
				column2=i; 
		if(column2!=column1) 
			swap_columns(M,column1,column2,row); 
	} 
} 
// Minimize the matrix from a given row to the bottom. 
// Compares with matrix vertices. 
int minimize_below ( matrix &M , int present_row , vector < int > *positions , bool true_if_equal ) 
{ 
	// If past the bottom row, return 0 
	if(present_row==M.size()) 
		return 0; 
	// Snapshot of positions 
	vector < int > snapshot(*positions); 
	// Try every vector in the given row 
	for(int i=present_row;i<M.size();++i) 
	{ 
		// Is it necessary to permute some rows? 
		if(snapshot[i]!=(*positions)[present_row]) 
		{ 
			// Yes it is. Put the right row vector in 
			// the present row. 
			for(int j=present_row+1;j<M.size();++j) 
			{
				if(snapshot[i]==(*positions)[j]) 
				{ 
					// Swap rows 
					vector < int > dummy_vector(M[present_row]); 
					M[present_row]=M[j]; 
					M[j]=dummy_vector; 
					int dummy_int=(*positions)[present_row]; 
					(*positions)[present_row]=(*positions)[j]; 
					(*positions)[j]=dummy_int; 
					break; 
				} 
			} 
		} 
		// Minimize present row (fixing the rows above) 
		minimize_row(M,present_row); 
		// Compare present row with same row in vertices. 
		int c=compare_points(vertices[d+present_row],M[present_row]); 
		// If the obtained represention is smaller, return 1 
		if(c==1) 
			return 1; 
		// If equal up to now, consider the rest. 
		if(c==0) 
		{ 
			int remain=minimize_below(M,present_row+1,positions,true_if_equal); 
			// If the rest is larger, return 1 
			if(remain==1) 
				return 1; 
			// If representations are equal, return true if 
			// we know vertices are in the minimal representation. 
			// (i.e. true_if_equal=true) 
			if(remain==0 && true_if_equal) 
				return 0; 
		} 
		// The obtained represention is larger. Try another. 
	} 
	return -1;
} 

// Compare two special embeddings 
// (other_vertices are not sorted by order) 
int compare_representation ( matrix &other_vertices , bool true_if_equal ) 
{ 
	vector < int > positions; 
	matrix reduced_vertices; 
	for(int i=0;i<other_vertices.size();++i) 
	{ 
		// Reduced vertices are without vertices in hyperplane one 
		if(add_entries(other_vertices[i])<1) 
		{ 
			positions.push_back(i); 
			reduced_vertices.push_back(other_vertices[i]); 
		} 
	} 
	// Minimize reduced_vertices from the top row 
	return minimize_below(reduced_vertices,0,&positions,true_if_equal); 
} 

// Returns true if face is contained in facet, and returns false if not. 
bool special_face_in_facet (vector < int > &special_face , vector < int > &facet_vertices) 
{ 
	for(int i=0;i<special_face.size();++i) 
		if(find(facet_vertices.begin(),facet_vertices.end(),special_face[i])==facet_vertices.end()) 
			return false; 
	return true; 
} 

// Returns true if ord( conv(vertices) ) = vertices 
// Otherwise false is returned. 
bool it_is_new ( vector < int > &sum_of_vertices ) 
{ 
	// Find special face 
	vector < int > special_face; 
	for(int i=0;i<d;++i) 
	{ 
		if(sum_of_vertices[i]<0) 
			return false; 
		if(sum_of_vertices[i]>0)
			special_face.push_back(i); 
	} 
	// Try every special embedding to see if ord(convV)=V 
	for(int facet_number=1;facet_number<facets.size();++facet_number) 
	{ 
		if(special_face_in_facet(special_face,facets[facet_number])) 
		{ 
			matrix new_representation; 
			matr_mult_matr(vertices,basechanges[facet_number],new_representation); 
			if(compare_representation(new_representation,true)==1) 
				return false; 
		} 
	} 
	// No strictly smaller special embedding 
	return true; 
} 

// GREATEST COMMON DIVISOR FUNCTIONS 
int gcd_pair ( int v1 , int v2 ) 
{ 
	while (v2) 
	{ 
		int k=v2; 
		v2=v1 % v2; 
		v1=k; 
	} 
	return v1; 
} 

int gcd ( vector <int> &v ) 
{ 
	if(v.empty()) 
		return 0; 
	int sfd=(v[0]<0 ? -v[0] : v[0]); 
	for(int i=1;i!=v.size();++i) 
	{ 
		int e=(v[i]<0 ? -v[i] : v[i]); 
		sfd=gcd_pair(sfd,e); 
	} 
	return sfd; 
} 

// False if lower bound exceeds upperbound. True otherwise. 
inline bool lower_below ( vector < int > &lowerbounds , vector < int > &upperbounds ) 
{ 
	for(int i=0;i<d;++i) 
		if(lowerbounds[i]>upperbounds[i]) 
			return false; 
	return true; 
} 

// AddPoint and its related functions 
inline bool first_entries_ok ( vector < int > &new_vertex , int h , vector < int > &lowerbounds , vector < int > &upperbounds , int position ) 
{ 
	// The first position entries have been chosen. 
	// Do the first entries violate the bounds? 
	for(int i=0;i<position;++i) 
		if(new_vertex[i]<lowerbounds[i] || new_vertex[i]>upperbounds[i]) 
			return false; 
	// Check bounds 
	if(lower_below(lowerbounds,upperbounds)==false) 
		return false; 
	// The first position entries have been chosen. Is it possible to hit hyperplane h 
	// staying inside the lower and upper bounds? If not, return false. 
	int partial_sum=accumulate(new_vertex.begin(),new_vertex.begin()+position+1,0); 
	if(accumulate(lowerbounds.begin()+position+1,lowerbounds.end(),0)+partial_sum>h) 
		return false; 
	if(accumulate(upperbounds.begin()+position+1,upperbounds.end(),0)+partial_sum<h) 
		return false; 
	// Yes it is. 
	return true; 
} 

bool next_vertex ( vector < int > &new_vertex , int h , int position , vector < int > &lowerbounds , vector < int > &upperbounds ) 
{ 
	// If all entries have been chosen, return true if gcd=1. 
	if(position==d)
	{ 
		if(gcd(new_vertex)==1) 
			return true; 
		else 
			return false; 
	} 
	// Find the interval [coord_begin,coord_end] the entry in position can be in. 
	int coord_begin=max(lowerbounds[position],new_vertex[position]); 
	int partial_sum=accumulate(new_vertex.begin(),new_vertex.begin()+position,0); 
	int lower_tail=accumulate(lowerbounds.begin()+position+1,lowerbounds.end(),0); 
	int coord_end=min(upperbounds[position],h-partial_sum-lower_tail); 
	// Run through this interval. 
	for(int coord=coord_begin;coord<=coord_end;++coord) 
	{ 
		// If the entry in position is equal to coord, 
		// how do the upper and lower bounds change? 
		vector < int > new_lowerbounds(lowerbounds); 
		if(neighboring_vertices[position]!=-1) 
		{ 
			int k=h+coord*(neighbors_in_hyperplane[position]-1); 
			for(int i=0;i<d;++i) 
			{ 
				if(i!=position) 
				{ 
					int c=(k==0 ? -1 : k)-coord*vertices[neighboring_vertices[position]][i]; 
					if(c>lowerbounds[i]) 
						new_lowerbounds[i]=c; 
				} 
			} 
		} 
		// If the entry in position is increased, 
		// then set the last entries to the lower bound. 
		if(coord>new_vertex[position]) 
			for(int i=position+1;i<d;++i) 
				new_vertex[i]=new_lowerbounds[i]; 
		// Set the entry in position equal to coord. 
		new_vertex[position]=coord;
		// Are the first entries ok? 
		if(first_entries_ok(new_vertex,h,new_lowerbounds,upperbounds,position)) 
		{ 
			// Can the last entries be chosen? 
			if(next_vertex(new_vertex,h,position+1,new_lowerbounds,upperbounds)) 
				return true; 
		} 
	} 
	// It is not possible to chose the last entries. 
	return false; 
} 

void addpoint() 
{ 
	// Compute the sum of the vertices 
	vector < int > sum_vertices; 
	add_rows(vertices,sum_vertices); 
	int sum_in_hyperplane=add_entries(sum_vertices); 
	// If convex hull is smooth Fano and 
	// special embedding minimal, output. 
	int number_of_facets=facets.size(); 
	if(conv_is_fano() && it_is_new(sum_vertices)) 
		output_vertices(); 
	remove_facets(number_of_facets); 
	// Determine which hyperplanes to place the next vertex in. 
	int h_begin=0; 
	if(vertices.size()>d) 
		h_begin=accumulate((vertices.back()).begin(),(vertices.back()).end(),0); 
	int h_end=-sum_in_hyperplane; 
	// Run through these hyperplanes. 
	for(int h=h_begin;h+sum_in_hyperplane>=0;--h) 
	{ 
		// Determine lower bounds on entries of the next vertex 
		vector < int > lowerbound(d,h-1);
		for(int i=0;i<d;++i) 
		{ 
			int k=h+lowerbound[i]*(neighbors_in_hyperplane[i]-1); 
			while(k>1 || (k==1 && lowerbound[i]<-1) || (k==1 && neighboring_facets[0][i]!=-1) || (sum_in_hyperplane+2*h<0 && lowerbound[i]+sum_vertices[i]<0)) 
			{ 
				++lowerbound[i]; 
				k+=neighbors_in_hyperplane[i]-1; 
			} 
		} 
		// Determine upper bounds on entries of the next vertex 
		vector < int > upperbound; 
		for(int i=0;i<d;++i) 
		{ 
			if(sum_in_hyperplane+2*h<0 && neighboring_facets[0][i]==-1) 
				upperbound.push_back(-1); 
			else 
				upperbound.push_back(h+sum_in_hyperplane-sum_vertices[i]+(h==0 && neighboring_facets[0][i]==-1 ? 1 : 0)); 
		} 
		// The lower bound cannot be greater than upper bound. 
		if(lower_below(lowerbound,upperbound)==false) 
			return; 
		// New vertex is set to lower bounds.... 
		vector < int > new_vertex(lowerbound); 
		// ... or previous vertex with last entry increased. 
		// (this happens if new vertex is in the same hyperplane 
		// as the previous) 
		if(h==accumulate((vertices.back()).begin(),(vertices.back()).end(),0)) 
		{ 
			new_vertex.assign((vertices.back()).begin(),(vertices.back()).end()); 
			++new_vertex[d-1]; 
		} 
		// Run through possible vertices in the hyperplane h 
		// in strictly increasing order. 
		while(next_vertex(new_vertex,h,0,lowerbound,upperbound))
		{ 
			// Add new vertex to vertex set 
			vertices.push_back(new_vertex); 
			// Is it a neighboring vertex to the initial facet? 
			for(int i=0;i<d;++i) 
			{ 
				if(new_vertex[i]==-1 && neighboring_vertices[i]==-1) 
				{ 
					neighboring_vertices[i]=vertices.size()-1; 
					neighbors_in_hyperplane[i]=h; 
				} 
			} 
			// If the new vertex set is compatible and no permutation of 
			// the set results in a smaller set, call addpoint recursively. 
			if(checksubset() && compare_representation(vertices,false)!=1) 
				addpoint(); 
			// Undo changes regarding neighboring vertices 
			for(int i=0;i<d;++i) 
			{ 
				if(neighboring_vertices[i]==vertices.size()-1) 
				{ 
					neighboring_vertices[i]=-1; 
					neighbors_in_hyperplane[i]=0; 
				} 
			} 
			// Undo changes to vertex set and facet 
			vertices.pop_back(); 
			remove_facets(number_of_facets); 
			// Increase last entry to ensure the next considered vertex 
			// is greater than the previous. 
			++new_vertex[d-1]; 
		} 
	} 
} 

// INITIALIZATION 
void sfp()
{ 
	// Add initial facet to set of facets 
	vector < int > initial_facet; 
	for(int i=0;i<d;++i) 
		initial_facet.push_back(i); 
	facets.push_back(initial_facet); 
	// Add normal 
	vector < int > one_vector(d,1); 
	normals.push_back(one_vector); 
	// Add identity matrix to basechanges 
	zero_vector.assign(d,0); 
	matrix unitmatrix(d,zero_vector); 
	for(int i=0;i<d;++i) 
		unitmatrix[i][i]=1; 
	basechanges.push_back(unitmatrix); 
	// No neighboring facets to begin with 
	minus_vector.assign(d,-1); 
	neighboring_facets.push_back(minus_vector); 
	// Set vertices to the standard basis 
	vertices.assign(unitmatrix.begin(),unitmatrix.end()); 
	// No neighboring vertices to begin with 
	neighboring_vertices.assign(d,-1); 
	neighbors_in_hyperplane.assign(d,0); 
	// Define constants 
	zero_matrix.assign(d,zero_vector); 
	// Call addpoint to begin the construction. 
	addpoint(); 
} 

// MAIN 
int main (int argc,char *argv[]) 
{ 
	// Read arguments.
	// Only one argument allowed: an integer between 2 and 9. 
	if(argc!=2) 
		return 0; 
	string st(argv[1]); 
	if(st.size()!=1) 
		return 0; 
	if(isdigit(st[0])==false) 
		return 0; 
	int d_char=st[0]; 
	d=d_char-48; 
	if(d<2) 
		return 0; 
	// Open output files 
	char outfile1[]="fanovertd"; 
	outfile1[8]=d_char; 
	fanovert.open(outfile1); 
	char outfile2[]="msgd"; 
	outfile2[3]=d_char; 
	msg.open(outfile2); 
	msg << endl << "SFP-algorithm: dimension " << d << endl << endl; 
	number_of_equivalence_classes.assign(3*d+1,0); 
	// Initialize and commence construction 
	sfp(); 
	// Final output 
	msg << "Construction finished!" << endl << endl; 
	msg << "Up to isomorphism, the program found" << endl << endl; 
	for(int i=d+1;i<3*d+1;++i) 
		msg << number_of_equivalence_classes[i] << " with " << i << " vertices ." << endl << endl; 
	msg << endl; 
	msg << "Total number of smooth Fano " << d << "-polytopes: " << (int) accumulate(number_of_equivalence_classes.begin(),number_of_equivalence_classes.end(),0) << endl << endl; 
	// Close files 
	fanovert.close(); 
	msg.close(); 
}
