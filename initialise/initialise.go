// Initialise is the main routine for the 'obro initialise' command

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package initialise

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/cache"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcas/obro/tables"
	"bitbucket.org/pcastools/log"
	"context"
)

// initialisePartial initialises the partial state table with state s, deleting all existing partial states.
func initialisePartial(conn *keyvalue.Connection, s *partial.State, lg log.Interface) error {
	// Connect to the partial state table
	t, err := conn.ConnectToTable(context.Background(), tables.PartialTableName(s.Dimension))
	if err != nil {
		return err
	}
	// Delete everything in the table
	n, err := t.Delete(context.Background(), record.Record{})
	if err != nil {
		t.Close()
		return err
	}
	lg.Printf("Deleted %d partial states.", n)
	// Create the record describing the initial partial state
	r, err := s.ToRecord()
	if err != nil {
		t.Close()
		return err
	}
	// Add the extra keys to the record
	r["beingProcessed"] = false
	r["ulid"] = cache.NewULID()
	r["timestamp"] = int64(0)
	// Insert the record in the table
	if err := t.InsertRecords(context.Background(), r); err != nil {
		t.Close()
		return err
	}
	// Close the table
	return t.Close()
}

// initialiseCompleted initialises the completed polytopes table, deleting all existing completed polytopes.
func initialiseCompleted(conn *keyvalue.Connection, dim int, lg log.Interface) error {
	// Connect to the completed table
	t, err := conn.ConnectToTable(context.Background(), tables.CompletedTableName(dim))
	if err != nil {
		return err
	}
	// Delete everything in the table
	n, err := t.Delete(context.Background(), record.Record{})
	if err != nil {
		return err
	}
	lg.Printf("Deleted %d completed polytopes.", n)
	// Close the table
	return t.Close()
}

// Initialise initialises the partial and completed key:value tables for the Obro algorithm, for dimension dim, deleting any existing data.
func Initialise(dim int, conn *keyvalue.Connection, _ metrics.Interface, lg log.Interface) error {
	// Create the initial partial state
	s, err := partial.Initial(dim)
	if err != nil {
		lg.Printf("Error creating initial partial state: %s", err)
		return err
	}
	// Initialise the tables
	if err := initialiseCompleted(conn, dim, lg); err != nil {
		lg.Printf("Error initialising completed polytopes table: %s", err)
		return err
	}
	if err := initialisePartial(conn, s, lg); err != nil {
		lg.Printf("Error initialising partial states table: %s", err)
		return err
	}
	lg.Printf("Inserted initial partial state for dimension %d.", dim)
	return nil
}
