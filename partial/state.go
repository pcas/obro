// State defines an object representing a partially-completed polytope

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package partial

import (
	"bitbucket.org/pcas/obro/matrix"
	"bitbucket.org/pcasmath/integer"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/slice"
	"bitbucket.org/pcastools/stringsbuilder"
	"fmt"
	"sync"
)

// State describes the data associated with a partially constructed polytope.
type State struct {
	Dimension              int                      // The dimension of the ambient space.
	Lattice                *integervector.Parent    // The lattice ZZ^dimension.
	Vertices               []*integervector.Element // The vertices of the polytope, as a slice of integer vectors.
	Facets                 [][]int                  // The facets of the polytope, where facets[i] is the slice containing the indices of vertices in the i-th facet.
	Normals                []*integervector.Element // The outward pointing normals to the facets.
	NeighbouringFacets     []map[int]int            // NeighbouringFacets[fid] is a map recording the facets that are known to neighbour the facet F with index fid. neighbouringFacets[fid][vid] records the facet id of the facet adjacent to F along the ridge of F specified by the vertex with index vid.
	Basechanges            []matrix.Matrix          // Basechanges[fid] is the change of basis matrix that maps the vertices of the facet F to the standard basis elements.
	NeighbouringVertices   map[int]int              // This specifies the neighbouring vertices to the initial (standard) facet. FIX ME am not exactly sure how yet.
	NeighboursInHyperplane []*integer.Element       // FIX ME I think this specifies which hyperplanes the vertices adjacent to the standard facet are in.
	ResumeFromHeight       *integer.Element         // The height from which to resume the search for the next vertex. If this is nil, we resume at the maximum possible height.
	ResumeFrom             *integervector.Element   // The place to resume the search for the next vertex. If this is nil, we resume at the lower bound.
	HasClosed              bool                     // True if and only if we have successfully closed the vertices of this state to a smooth Fano polytope.
}

// statePool is a pool of *State objects available for use.
var statePool = &sync.Pool{
	New: func() interface{} {
		return &State{}
	},
}

//////////////////////////////////////////////////////////////////////
// State functions
//////////////////////////////////////////////////////////////////////

// Initial returns the initial state for the search in dimension d.
func Initial(d int) (*State, error) {
	// Fetch the ambient lattice (this doubles as a check on the dimension)
	zZn, err := integervector.DefaultLattice(d)
	if err != nil {
		return nil, err
	}
	// Initialise the data we need
	nf := integervector.ConstantVector(zZn, integer.One())
	id := matrix.IdentityMatrix(d)
	seqSlice := make([]int, d)
	for i := 0; i < d; i++ {
		seqSlice[i] = i
	}
	// Create and return the state
	s := statePool.Get().(*State)
	s.Dimension = d
	s.Lattice = zZn
	s.Vertices = id.Columns()
	s.Facets = [][]int{seqSlice}
	s.Normals = []*integervector.Element{nf}
	s.NeighbouringFacets = []map[int]int{make(map[int]int)}
	s.Basechanges = []matrix.Matrix{id}
	s.NeighbouringVertices = make(map[int]int)
	s.NeighboursInHyperplane = integer.ZeroSlice(d)
	return s, nil
}

// String returns a string description of the state.
func (s *State) String() string {
	if s == nil {
		return "<nil>"
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	fmt.Fprintf(b, "Dimension: %d\n", s.Dimension)
	fmt.Fprintf(b, "Vertices:\n")
	for i, v := range s.Vertices {
		fmt.Fprintf(b, "\t%d: %s\n", i, v)
	}
	fmt.Fprintf(b, "Facets:\n")
	for i, F := range s.Facets {
		if s.Normals == nil {
			fmt.Fprintf(b, "\t%d: %v\n", i, F)
		} else {
			fmt.Fprintf(b, "\t%d: %v with normal %s\n", i, F, s.Normals[i])
		}
	}
	fmt.Fprintf(b, "Neighbouring facets:\n")
	for i, nf := range s.NeighbouringFacets {
		fmt.Fprintf(b, "\t%d: %v\n", i, nf)
	}
	fmt.Fprintf(b, "Basechanges:\n")
	for i, bc := range s.Basechanges {
		fmt.Fprintf(b, "\t%d: %s\n", i, bc)
	}
	fmt.Fprintf(b, "Neighbouring vertices: %v\n", s.NeighbouringVertices)
	fmt.Fprintf(b, "Neighbours in hyperplane: %s\n", s.NeighboursInHyperplane)
	fmt.Fprintf(b, "Resume from height: %s\n", s.ResumeFromHeight)
	fmt.Fprintf(b, "Resume from vertex: %s", s.ResumeFrom)
	return b.String()
}

// Reset resets s ready for reuse. Returns s.
func (s *State) Reset() *State {
	if s != nil {
		s.Dimension = 0
		s.Lattice = nil
		s.Vertices = nil
		s.Facets = nil
		s.Normals = nil
		s.NeighbouringFacets = nil
		s.Basechanges = nil
		s.NeighbouringVertices = nil
		s.NeighboursInHyperplane = nil
		s.ResumeFromHeight = nil
		s.ResumeFrom = nil
		s.HasClosed = false
	}
	return s
}

// Copy returns a copy of s.
func (s *State) Copy() *State {
	// Sanity check
	if s == nil {
		return s
	}
	// Grab a new *State from the pool
	t := statePool.Get().(*State)
	// Copy the easy parts
	t.Dimension = s.Dimension
	t.Lattice = s.Lattice
	t.Vertices = integervector.CopySlice(s.Vertices)
	t.Normals = integervector.CopySlice(s.Normals)
	t.NeighboursInHyperplane = integer.CopySlice(s.NeighboursInHyperplane)
	t.ResumeFromHeight = s.ResumeFromHeight
	t.ResumeFrom = s.ResumeFrom
	t.HasClosed = s.HasClosed
	// Copy the more complicated parts
	// s.Facets
	facets := make([][]int, 0, len(s.Facets))
	for _, val := range s.Facets {
		facets = append(facets, slice.CopyInt(val))
	}
	t.Facets = facets
	// s.NeighbouringFacets
	neighbouringFacets := make([]map[int]int, 0, len(s.NeighbouringFacets))
	for _, mapval := range s.NeighbouringFacets {
		mapvalcpy := make(map[int]int, len(mapval))
		for k, v := range mapval {
			mapvalcpy[k] = v
		}
		neighbouringFacets = append(neighbouringFacets, mapvalcpy)
	}
	t.NeighbouringFacets = neighbouringFacets
	// s.Basechanges
	t.Basechanges = make([]matrix.Matrix, len(s.Basechanges))
	copy(t.Basechanges, s.Basechanges)
	// s.NeighbouringVertices
	neighbouringVertices := make(map[int]int, len(s.NeighbouringVertices))
	for k, v := range s.NeighbouringVertices {
		neighbouringVertices[k] = v
	}
	t.NeighbouringVertices = neighbouringVertices
	// Return the copy
	return t
}

// Delete makes s available for future reuse. You must not modify s after calling Delete.
func (s *State) Delete() {
	if s != nil {
		statePool.Put(s.Reset())
	}
}
