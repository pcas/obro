// Record contains functions for converting partial state data to/from a record.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package partial

import (
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/obro/matrix"
	"bitbucket.org/pcasmath/integer"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/gobutil"
	"errors"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// sharesRidge returns true, k if the slices S1 and S2 differ only at place k, and false, -1 otherwise
func sharesRidge(S1 []int, S2 []int) (bool, int) {
	if len(S1) != len(S2) {
		return false, -1
	}
	difIdx := -1
	for i, s1 := range S1 {
		if S2[i] != s1 {
			if difIdx != -1 {
				return false, -1
			}
			difIdx = i
		}
	}
	return difIdx != -1, difIdx
}

// makeBasisChangeMatrix returns a matrix M' defined as follows. If M is such that v_i*M = e_i, where v_1,...,v_n are vertices of a facet (as row vectors) and e_1,...,e_n are the standard basis vectors (as row vectors) then v'_i M' = e_i where v'_i = v_i if i ne k and v'_k = v.
func makeBasisChangeMatrix(M matrix.Matrix, v *integervector.Element, k int) matrix.Matrix {
	rel := matrix.VectorMultiplyMatrix(v, M).ToIntegerSlice()
	rel[k] = integer.FromInt(-2)
	res := M.Columns() // Starts off as the columns of M and we then correct it
	kcol := M.Column(k)
	for j, r := range rel {
		u, err := integervector.ScalarMultiplyByIntegerThenAdd(r, kcol, res[j])
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		res[j] = u
	}
	return matrix.Matrix(res)
}

// rebuildBasisChangesAndNormals rebuilds the basis changes and normals of the partial state data.
func rebuildBasisChangesAndNormals(s *State) error {
	// Construct the base changes
	verts := s.Vertices
	cob := make([]matrix.Matrix, 0, len(s.Facets))
	for i, F := range s.Facets {
		// The basis change matrix for the first facet is trivial
		if i == 0 {
			cob = append(cob, matrix.IdentityMatrix(s.Dimension))
			continue
		}
		// Find a facet G sharing a ridge F
		success := false
		for j, G := range s.Facets[:i] {
			if ok, k := sharesRidge(F, G); ok {
				success = true
				M := makeBasisChangeMatrix(cob[j], verts[F[k]], k)
				cob = append(cob, M)
				break
			}
		}
		// Sanity check
		if !success {
			return fmt.Errorf("no facet adjacent to facet %d", i)
		}
	}
	// Construct the normals
	zZn := s.Lattice
	ns := make([]*integervector.Element, 0, len(cob))
	for _, M := range cob {
		ns = append(ns, matrix.VectorFromIntegerSlice(zZn, M.RowSums()))
	}
	// Set the data
	s.Basechanges = cob
	s.Normals = ns
	return nil
}

// toInt64Slice returns a slice of int64s encoding the dimension, lattice, vertices, and neighboursInHyperplane fields of the given partial state.
func toInt64Slice(s *State) ([]int64, error) {
	// Note the dimension and number of vertices, and allocate the slice
	d, n := s.Dimension, len(s.Vertices)
	result := make([]int64, 0, 2+n*d+len(s.NeighboursInHyperplane))
	// Add dimension and number of vertices to the slice
	result = append(result, int64(d))
	result = append(result, int64(n))
	// Add the vertices to the slice
	for _, v := range s.Vertices {
		S, err := v.ToInt64Slice()
		if err != nil {
			// We have assumed that the entries fit in an int64
			return nil, fmt.Errorf("the assumption that the vertices of a smooth Fano polytope have small entries has failed: %w", err)
		}
		result = append(result, S...)
	}
	// Add the neighboursInHyperplane data
	for _, h := range s.NeighboursInHyperplane {
		H, err := h.Int64()
		if err != nil {
			// We have assumed that the entries fit in an int64
			return nil, fmt.Errorf("the hyperplane neighbours is impossibly large: %w", err)
		}
		result = append(result, H)
	}
	return result, nil
}

// fromInt64Slice returns partial state data populated with dimension, lattice, vertices, and neighboursInHyperplane decoded from S.
func fromInt64Slice(S []int64) (*State, error) {
	// Sanity check
	if len(S) < 2 {
		return nil, errors.New("malformed slice")
	}
	// The dimension and lattice
	d := int(S[0])
	zZn, err := integervector.DefaultLattice(d)
	if err != nil {
		return nil, fmt.Errorf("unable to create default lattice of dimension %d", d)
	}
	// The number of vertices
	n := int(S[1])
	S = S[2:]
	if len(S) < n*d {
		return nil, errors.New("malformed slice")
	}
	// The vertices
	verts := make([]*integervector.Element, 0, n)
	for i := 0; i < n; i++ {
		v, err := integervector.FromInt64Slice(zZn, S[:d])
		if err != nil {
			return nil, fmt.Errorf("error reading vertices: %w", err)
		}
		verts = append(verts, v)
		S = S[d:]
	}
	// The neighboursInHyperplane
	neighbours := make([]*integer.Element, 0, len(S))
	for _, x := range S {
		neighbours = append(neighbours, integer.FromInt64(x))
	}
	// Create and return the state data
	s := statePool.Get().(*State)
	s.Dimension = d
	s.Lattice = zZn
	s.Vertices = verts
	s.NeighboursInHyperplane = neighbours
	return s, nil
}

// integerToStringOrNil returns the string value of the integer x if x is non-nil, or "nil" if x is nil
func integerToStringOrNil(x *integer.Element) string {
	if x == nil {
		return "nil"
	}
	return x.String()
}

// stringToIntegerOrNil returns either nil if the string is "nil", or an integer if the string is not equal to "nil".
func stringToIntegerOrNil(s string) (*integer.Element, error) {
	if s == "nil" {
		return nil, nil
	}
	return integer.FromString(s)
}

// integerVectorToStringOrNil returns the string value of the integer vector x if x is non-nil, or "nil" if x is nil
func integerVectorToStringOrNil(x *integervector.Element) string {
	if x == nil {
		return "nil"
	}
	return x.String()
}

// stringToIntegerVectorOrNil returns either nil if the string is "nil", or an integer vector (in the lattice zZn) if the string is not equal to "nil".
func stringToIntegerVectorOrNil(s string, zZn *integervector.Parent) (*integervector.Element, error) {
	if s == "nil" {
		return nil, nil
	}
	return integervector.FromString(zZn, s)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToRecord returns a record representing the partial state data. The returned record will have key:value entries:
//		"encodedState": []byte	// Gob-encoded data describing the state
//		"hasClosed":    bool	// Does this close to give a polytope?
func (s *State) ToRecord() (record.Record, error) {
	// Sanity check
	if s == nil {
		return nil, errors.New("illegal nil state")
	}
	// Create a gob-encoder
	enc := gobutil.NewEncoder()
	defer gobutil.ReuseEncoder(enc)
	// Gob-encode almost all the data
	if S, err := toInt64Slice(s); err != nil {
		return nil, fmt.Errorf("error generating numerical data: %w", err)
	} else if err = enc.Encode(S); err != nil {
		return nil, fmt.Errorf("error encoding numerical data: %w", err)
	} else if err = enc.Encode(s.Facets); err != nil {
		return nil, fmt.Errorf("error encoding facets: %w", err)
	} else if err = enc.Encode(s.NeighbouringFacets); err != nil {
		return nil, fmt.Errorf("error encoding neighbouring facets: %w", err)
	} else if err = enc.Encode(s.NeighbouringVertices); err != nil {
		return nil, fmt.Errorf("error encoding neighbouring vertices: %w", err)
	} else if err = enc.Encode(integerToStringOrNil(s.ResumeFromHeight)); err != nil {
		return nil, fmt.Errorf("error encoding resume height: %w", err)
	} else if err = enc.Encode(integerVectorToStringOrNil(s.ResumeFrom)); err != nil {
		return nil, fmt.Errorf("error encoding resume vector: %w", err)
	}
	// Return the record.
	return record.Record{
		"encodedState": enc.Bytes(),
		"hasClosed":    s.HasClosed,
	}, nil
}

// FromRecord returns the partial state data represented by the given record. The record must have key:value entries:
//		"encodedState": []byte	// Gob-encoded data describing the state
//		"hasClosed":    bool	// Does this close to give a polytope?
func FromRecord(r record.Record) (*State, error) {
	// Does r have the required keys?
	for _, k := range [...]string{"encodedState", "hasClosed"} {
		if _, ok := r[k]; !ok {
			return nil, fmt.Errorf("record missing key \"%s\"", k)
		}
	}
	// Create a decoder for the gob-encoded state data
	b, ok := r["encodedState"].([]byte)
	if !ok {
		return nil, fmt.Errorf("type for \"encodedState\" should be []byte (found %T)", r["encodedState"])
	}
	dec := gobutil.NewDecoder(b)
	// Extract and decode the numerical data
	var S []int64
	if err := dec.Decode(&S); err != nil {
		return nil, fmt.Errorf("error decoding numerical data: %w", err)
	}
	// Decode the numerical data into a partial state
	var str string
	s, err := fromInt64Slice(S)
	if err != nil {
		return nil, fmt.Errorf("error decoding numerical data: %w", err)
	}
	// Populate the rest of the gob-encoded data
	if err = dec.Decode(&s.Facets); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding facets: %w", err)
	} else if err = dec.Decode(&s.NeighbouringFacets); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding neighbouring facets: %w", err)
	} else if err = dec.Decode(&s.NeighbouringVertices); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding neighbouring vertices: %w", err)
	} else if err = dec.Decode(&str); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding resume height: %w", err)
	} else if s.ResumeFromHeight, err = stringToIntegerOrNil(str); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding resume height: %w", err)
	} else if err = dec.Decode(&str); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding resume vector: %w", err)
	} else if s.ResumeFrom, err = stringToIntegerVectorOrNil(str, s.Lattice); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error decoding resume vector: %w", err)
	}
	// Has the partial polytope closed to a polytope?
	hasClosed, ok := r["hasClosed"].(bool)
	if !ok {
		s.Delete()
		return nil, fmt.Errorf("type for \"hasClosed\" should be bool (found %T)", r["hasClosed"])
	}
	s.HasClosed = hasClosed
	// Rebuild the basis change matrices and normals
	if err = rebuildBasisChangesAndNormals(s); err != nil {
		s.Delete()
		return nil, fmt.Errorf("error rebuilding basis change matrices: %w", err)
	}
	return s, nil
}
