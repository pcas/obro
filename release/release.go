// Release is the main routine for the 'obro release' command

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package release

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/tables"
	"bitbucket.org/pcastools/log"
	"context"
)

// Release marks all states in the partial key:value table of dimension dim as not being processed.
func Release(dim int, conn *keyvalue.Connection, _ metrics.Interface, lg log.Interface) error {
	// Connect to the partial state table
	t, err := conn.ConnectToTable(context.Background(), tables.PartialTableName(dim))
	if err != nil {
		lg.Printf("Error releasing partial states: %s", err)
		return err
	}
	// Release any partial states that are marked as being processed
	selector := record.Record{
		"beingProcessed": true,
	}
	update := record.Record{
		"beingProcessed": false,
		"timestamp":      int64(0),
	}
	n, err := t.Update(context.Background(), selector, update)
	if err != nil {
		t.Close()
		lg.Printf("Error releasing partial states: %s", err)
		return err
	}
	lg.Printf("Released %d partial states.", n)
	// Close the table
	if err := t.Close(); err != nil {
		lg.Printf("Error releasing partial states: %s", err)
		return err
	}
	return nil
}
