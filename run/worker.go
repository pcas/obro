// Worker provides functions to launch the worker that runs the main computation.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package run

import (
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcas/obro/search"
	"bitbucket.org/pcastools/log"
	"sync"
)

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// merge merges the channels cs into a single channel out.  It returns the channel out.  The channel out will close once all the channels in cs are closed.  This is adapted from "Go Concurrency Patterns: Pipelines and Cancellation" by Sameer Ajmani, blog.golang.org, 13 March 2014.
func merge(cs ...chan interface{}) chan interface{} {
	out := make(chan interface{})
	var wg sync.WaitGroup
	output := func(c <-chan interface{}) {
		for x := range c {
			out <- x
		}
		wg.Done()
	}
	// launch goroutines to copy the cs into out
	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}
	// launch a goroutine to close out once all the cs are closed
	go func() {
		wg.Wait()
		close(out)
	}()
	// return the output channel
	return out
}

//////////////////////////////////////////////////////////////////////
// Worker functions
//////////////////////////////////////////////////////////////////////

// launchWorkers launches a group of workers of the given size with the given input channel.  It returns the output channels for partial states and completed polytopes, and a channel that should be closed to shut down the workers.
func launchWorkers(num int, inputC <-chan *partial.State, lg log.Interface) (partialC chan interface{}, completedC chan interface{}, doneC chan struct{}) {
	// Sanity checks
	if num < 1 {
		panic("The number of workers must be a positive integer")
	}
	if lg == nil {
		lg = log.Discard
	}
	// Make the done channel
	doneC = make(chan struct{})
	// launch the workers
	completedOutputs := make([]chan interface{}, num)
	partialOutputs := make([]chan interface{}, num)
	for i := 0; i < num; i++ {
		completedOutputs[i] = make(chan interface{}, 5)
		partialOutputs[i] = make(chan interface{}, 5)
		wlg := log.PrefixWith(lg, "[worker %d]", i+1)
		go search.Worker(inputC, completedOutputs[i], partialOutputs[i], doneC, wlg)
	}
	// merge the worker output channels
	partialC = merge(partialOutputs...)
	completedC = merge(completedOutputs...)
	return
}
