// Cache provides cache functions for the main computation

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package run

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/cache"
	"bitbucket.org/pcas/obro/hpc"
	"bitbucket.org/pcas/obro/tables"
	"bitbucket.org/pcastools/log"
	"context"
	"time"
)

// The maximum size of the partial state and completed polytopes caches.
const (
	partialCacheSize   = 20000
	completedCacheSize = 5000
)

// cacheMetricInterval determines how frequently metrics are collected about a cache.
const cacheMetricInterval = 10 * time.Second

// maxNumRespawn is the maximum number of times the job will be respawned.
const maxNumRespawn = 1

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// respawn returns a callback function that will respawn the job, using the given respawn command, at most maxNumRespawn times.
func respawn(respawnCommand string) cache.CallbackFunc {
	// deal with the trivial case
	if respawnCommand == "" {
		return nil
	}
	// return the callback function
	num := 0
	return func(lg log.Interface) error {
		// Shall we respawn?
		if num < maxNumRespawn {
			lg.Printf("Respawning")
			hpc.Spawn(lg, respawnCommand)
			num++
		}
		return nil
	}
}

// cacheMetricWorker sends a *metrics.Point with name "obro.CacheSize", field "Size" and value the value of Len to the given metrics endpoint every cacheMetricInterval. Collection will stop when the shutdownC channel is closed. Intended to be run in its own go routine.
func cacheMetricWorker(Len func() int, shutdownC <-chan struct{}, met metrics.Interface) {
	t := time.NewTicker(cacheMetricInterval)
	for {
		select {
		case <-t.C:
			pt, _ := metrics.NewPoint("obro.CacheSize", metrics.Fields{"Size": Len()}, metrics.Tags{})
			met.Submit(context.Background(), pt)
		case <-shutdownC:
			return
		}
	}
}

// createCaches creates and returns the completed polytopes cache and partial state cache. It will also return a channel that should be closed to stop collecting metrics data about the caches.
func createCaches(dim int, respawnCommand string, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) (*cache.Partial, *cache.Completed, chan<- struct{}, error) {
	// Create the partial state cache
	partialTable, err := conn.ConnectToTable(context.Background(), tables.PartialTableName(dim))
	if err != nil {
		return nil, nil, nil, err
	}
	partialCache := cache.NewPartial(partialCacheSize, partialTable)
	// Create the completed polytopes cache
	completedTable, err := conn.ConnectToTable(context.Background(), tables.CompletedTableName(dim))
	if err != nil {
		return nil, nil, nil, err
	}
	completedCache := cache.NewCompleted(completedCacheSize, completedTable)
	// Set the loggers on the caches
	partialCache.SetLogger(log.PrefixWith(lg, "[Partial Cache]"))
	completedCache.SetLogger(log.PrefixWith(lg, "[Completed Cache]"))
	// Install the respawn function on the partial cache
	partialCache.AddFlushCallback(respawn(respawnCommand))
	// Start collecting metrics about the cache sizes
	cacheMetricsC := make(chan struct{})
	go cacheMetricWorker(partialCache.Len, cacheMetricsC, metrics.TagWith(met, metrics.Tags{"Cache": "Partial"}))
	go cacheMetricWorker(completedCache.Len, cacheMetricsC, metrics.TagWith(met, metrics.Tags{"Cache": "Completed"}))
	// Return the caches
	return partialCache, completedCache, cacheMetricsC, nil
}

// closeCaches will close the completed polytopes cache and partial state cache. It will also close the cacheMetricsC channel, which stops collecting metrics data about the caches. Returns any errors.
func closeCaches(partialCache *cache.Partial, completedCache *cache.Completed, cacheMetricsC chan<- struct{}) (err error) {
	// Stop collecting cache metrics
	close(cacheMetricsC)
	// Close the caches (note that the order in which we do this is important)
	if err = completedCache.Close(); err != nil {
		partialCache.MarkAsFailed()
	}
	if e := partialCache.Close(); err == nil {
		err = e
	}
	return
}
