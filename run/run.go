// Run is the main routine for the 'obro run' command

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package run

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/cache"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcastools/log"
	"runtime"
	"time"
)

// numWorkers is the number of workers to launch
var numWorkers int = runtime.GOMAXPROCS(0)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// errorListener listens for errors on errC and logs them to lg.  It exits when errC closes.
func errorListener(errC <-chan error, lg log.Interface) {
	for err := range errC {
		lg.Printf("Got error: %v", err)
	}
	lg.Printf("Exiting")
}

// feedCacheToInput takes partial states from partialCache and passes them down the worker input channel inputC.  Any errors are passed down errC.  feedCacheToInput exits after runDuration.
func feedCacheToInput(partialCache *cache.Partial, inputC chan<- *partial.State, errC chan<- error, runDuration time.Duration, lg log.Interface) {
	timeout := time.After(runDuration)
	ok := true
	for ok {
		// found records whether or not we currently have a partial polytope
		found := false
		// s is the partial polytope that we hold
		var s *partial.State
		var x interface{}
		var err error
		select {
		case <-timeout:
			lg.Printf("A timeout occurred.  Exiting.")
			ok = false
		case x, ok = <-partialCache.C:
			if ok {
				// we successfully read a job from the cache
				s = x.(*partial.State)
				found = true
			}
		}
		// if we found a partial polytope, feed it to the worker input channel inputC
		if found && ok {
			select {
			case inputC <- s:
				// we successfully fed the partial polytope to the workers
			case <-timeout:
				// push the partial polytope back onto the cache
				if err = partialCache.Add(s); err != nil {
					lg.Printf("Error when trying to re-cache this partial polytope: %+v", err)
					errC <- err
				}
				// record that we are exiting
				ok = false
			}
		}
	}
}

// Run runs the Obro algorithm in dimension dim
func Run(dim int, respawnCommand string, runDuration time.Duration, conn *keyvalue.Connection, met metrics.Interface, lg log.Interface) error {
	// Create the completed polytopes cache and partial state cache
	partialCache, completedCache, cacheMetricsC, err := createCaches(dim, respawnCommand, conn, met, lg)
	if err != nil {
		return err
	}
	// Make the worker input channel and launch the workers
	inputC := make(chan *partial.State)
	partialC, completedC, workerDoneC := launchWorkers(numWorkers, inputC, lg)
	// Launch the listeners
	errC := make(chan error)
	listenerDoneC := make(chan struct{}, 2)
	go partialListener(partialC, partialCache, errC, listenerDoneC, log.PrefixWith(lg, "[partialListener]"))
	go completedListener(completedC, completedCache, errC, listenerDoneC, log.PrefixWith(lg, "[completedListener]"))
	go errorListener(errC, log.PrefixWith(lg, "[errorListener]"))
	// Loop until a timout occurs, reading partial states from
	// the partial cache and passing them to the workers
	feedCacheToInput(partialCache, inputC, errC, runDuration, lg)
	// A timeout has occurred, or the calculation has ended, or an error has occurred. We exit gracefully.
	lg.Printf("Starting shutdown dance")
	// Closing the done channel shuts down the workers, and hence the listeners
	close(workerDoneC)
	// wait for the listeners to flush their caches and exit
	<-listenerDoneC
	<-listenerDoneC
	// close the error channel, thereby shutting down the error listener
	close(errC)
	// Close the caches and provide feedback
	err = closeCaches(partialCache, completedCache, cacheMetricsC)
	if err != nil {
		lg.Printf("Error when processing: %s", err)
	} else {
		lg.Printf("Processing completed.")
	}
	return err
}
