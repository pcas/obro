// Listener provides listener functions that co-ordinate the main computation.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package run

import (
	"bitbucket.org/pcas/obro/cache"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/log"
)

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// partialListener reads partial states from the channel c and writes them to the cache.  If an error occurs, it is fed down the channel errC.  listener exits when c is closed, flushing the cache before it does so and then sending down doneC.  It logs error messages to lg.
func partialListener(c <-chan interface{}, partialCache *cache.Partial, errC chan<- error, doneC chan<- struct{}, lg log.Interface) {
	for x := range c {
		if err := partialCache.Add(x.(*partial.State)); err != nil {
			lg.Printf("Got error: %+v", err)
			errC <- err
		}
	}
	lg.Printf("listener about to exit.  Flushing %v polytopes.", partialCache.Len())
	// flush the cache on exit
	if err := partialCache.Close(); err != nil {
		lg.Printf("Got error on cache close: %+v", err)
		errC <- err
	}
	// signal that we are done
	lg.Printf("listener exited.")
	doneC <- struct{}{}
}

// completedListener reads completed polytopes from the channel c and writes them to the cache.  If an error occurs, it is fed down the channel errC.  listener exits when c is closed, flushing the cache before it does so and then sending down doneC.  It logs error messages to lg.
func completedListener(c <-chan interface{}, completed *cache.Completed, errC chan<- error, doneC chan<- struct{}, lg log.Interface) {
	for x := range c {
		if err := completed.Add(x.([]*integervector.Element)); err != nil {
			lg.Printf("Got error: %+v", err)
			errC <- err
		}
	}
	lg.Printf("listener about to exit.  Flushing %v polytopes.", completed.Len())
	// flush the cache on exit
	if err := completed.Close(); err != nil {
		lg.Printf("Got error on cache close: %+v", err)
		errC <- err
	}
	// signal that we are done
	lg.Printf("listener exited.")
	doneC <- struct{}{}
}
