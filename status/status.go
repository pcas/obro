// Status is the main routine for the 'obro status' command

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package status

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/obro/tables"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
)

// Status counts the number of entries in the partial and completed key:value tables in dimension dim.
func Status(dim int, conn *keyvalue.Connection, _ metrics.Interface, lg log.Interface) error {
	var numPartial, numCompleted int64
	// Connect to the partial table and perform the count
	if t, err := conn.ConnectToTable(context.Background(), tables.PartialTableName(dim)); err != nil {
		lg.Printf("Error connecting to table of partial polytopes: %v", err)
		return err
	} else if numPartial, err = t.Count(context.Background(), nil); err != nil {
		lg.Printf("Error when counting partial polyopes: %v", err)
		return err
	}
	// Connect to the completed table and perform the count
	if t, err := conn.ConnectToTable(context.Background(), tables.CompletedTableName(dim)); err != nil {
		lg.Printf("Error connecting to table of completed polytopes: %v", err)
		return err
	} else if numCompleted, err = t.Count(context.Background(), nil); err != nil {
		lg.Printf("Error when counting completed polyopes: %v", err)
		return err
	}
	// Log and print the counts
	lg.Printf("Current counts in dimension %d are:\nCompleted: %d\nPartial: %d", dim, numCompleted, numPartial)
	fmt.Printf("Completed: %d\nPartial: %d\n", numCompleted, numPartial)
	return nil
}
