// Tables defines functions to connect to and close the key:value tables

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package tables

import (
	"fmt"
)

// The partial state and completed table names. Here '%d' is a placeholder for the dimension.
const (
	partialTableName   = "partial_%d"
	completedTableName = "completed_%d"
)

// PartialTableName returns the name of the table of partial states.
func PartialTableName(dim int) string {
	return fmt.Sprintf(partialTableName, dim)
}

// CompletedTableName returns the name of the table of completed polytopes.
func CompletedTableName(dim int) string {
	return fmt.Sprintf(completedTableName, dim)
}
