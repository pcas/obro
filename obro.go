// Obro is a distributed implementation of Obro's algorithm for classifying Fano polytopes.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package main

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/runtimemetrics"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"context"

	_ "bitbucket.org/pcas/hpc"
	_ "bitbucket.org/pcas/keyvalue/kvdb/handler/kvdb"
)

/*
we represent polytopes as follows:

vertices are integer vectors.  they have indices that are ints.
facets are slices of d ints, which record the indices of the vertices in that facet (recall that each facet is simplicial)
ridges are codimension-2 faces; they are recorded as slices of (d-1) ints which specify the indices of vertices in that ridge.  Note that a pair (fid, vid), where vid is the index of a vertex v in the face F specified by fid, determines a ridge of F.  This ridge contains all vertices of F other than v; we call it the ridge of F specified by v.

*/

func main() {
	// Run cleanup functions on exit
	defer cleanup.Run()
	// Parse the options
	opts := setOptions()
	// Grab the logger and the metrics endpoint
	lg := log.Log()
	met := metrics.Metrics()
	// Connect to the database
	url := kvdb.DefaultConfig().URL(DatabaseName)
	conn, err := keyvalue.Open(context.Background(), url)
	assertNoErr(err)
	defer conn.Close()
	// Add runtime metrics, stopping them on exit
	cleanup.Add(runtimemetrics.Start(met).Stop)
	// Run the command
	if f := opts.Run; f != nil {
		if err = f(opts.Dimension, conn, met, lg); err != nil {
			lg.Printf("Error running command: %v", err)
		}
	}
	// Handle any errors
	assertNoErr(err)
}
