// Error defines error constants for working with a cache.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"strconv"
)

// cacheError is a constant error.
type cacheError int

// Errors used to communicate information about the global partial state table.
const (
	errNoPartialStatesAvailable cacheError = iota
	errCalculationFinished
	errMarkedAsFailed
)

/////////////////////////////////////////////////////////////////////////
// cacheError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e cacheError) Error() string {
	switch e {
	case errNoPartialStatesAvailable:
		return "No available partial states"
	case errCalculationFinished:
		return "Calculation finished"
	case errMarkedAsFailed:
		return "Cache placed in failed state"
	}
	return "Unknown error [" + strconv.Itoa(int(e)) + "]"
}
