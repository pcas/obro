// Partial provides a cache for partial state data.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcastools/cache"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/ulid"
	"context"
	"errors"
	"fmt"
	"sync"
	"time"
)

// Partial is a cache for storing partial state data.
type Partial struct {
	*Base
	C <-chan *partial.State // Partial states can be pulled from the cache via this channel. This channel will be closed when the cache is closed. Note that a cache is unordered: the order in which partial states are recovered from the cache via this channel has no relation to the order in which they were added to the cache.

	m        sync.RWMutex // Controls access to the following
	isClosed bool         // Have we closed?
	closeErr error        // The error on close (if any)

	ulidm sync.Mutex  // Controls access to the slice of ULIDs
	ulids []ulid.ULID // The slice of ULIDs

	shutdownC chan<- struct{} // Close to shut down the worker
	doneC     <-chan struct{} // Closed by the worker on exit
}

// pollInterval is how often to query the partial state table if the cache is empty.
const pollInterval = 10 * time.Second

// maxFetchSize is the maximum number of partial states to attempt to fetch from the partial state table.
const maxFetchSize = 500

// fetchDelta is the number of additional partial states to fetch from the partial state table. Increasing this value will increase the probability of fetching the desired number of partial states, at the cost of increasing the amount of data sent from the database.
const fetchDelta = 5

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// partialIterator returns an iterator for keyvalue.Records built from the slice S of partial state data. The records will have key:value entries:
//		"encodedState":   []byte	// Gob-encoded data describing the state
//		"hasClosed":      bool		// Does this close to give a polytope?
//		"beingProcessed": bool		// Is this partial state being processed?
//		"ulid":           string	// The ULID in the table
//		"timestamp":      int64		// The timestamp for being processed
func partialIterator(S []interface{}) record.Iterator {
	return record.SliceIteratorWithConversionFunc(S, func(x interface{}) (record.Record, error) {
		// Convert x to a partial state
		s, ok := x.(*partial.State)
		if !ok {
			return nil, fmt.Errorf("unable to coerce slice element: %T", x)
		}
		// Convert the partial state s to a record
		r, err := s.ToRecord()
		if err != nil {
			return nil, err
		}
		// Add the extra keys to the record and return
		r["beingProcessed"] = false
		r["ulid"] = NewULID()
		r["timestamp"] = int64(0)
		return r, nil
	})
}

// markPartial attempts to mark the partial state given by the ULID in the partial state table t as being processed, with timestamp ts. This will return true iff
//  i) there exists exactly one partial state in the table t with matching ULID;
//	ii) that partial state is not currently marked as being processed; and
//	iii) the update successfully marked it as being processed.
// That is, a return value of true guarantees that the caller owns the partial state data.
func markPartial(t *keyvalue.Table, u ulid.ULID, ts time.Time) (bool, error) {
	// Attempt to mark this partial state as being processed
	selector := record.Record{
		"ulid":           u.String(),
		"beingProcessed": false,
	}
	update := record.Record{
		"beingProcessed": true,
		"timestamp":      ts.Unix(),
	}
	n, err := t.Update(context.Background(), update, selector)
	if err != nil {
		return false, err
	}
	// Sanity check -- if we have updated more than one partial state then the ULID was not unique. This is a disaster; you will need to restart the computation from the beginning.
	if n > 1 {
		return false, fmt.Errorf("expected to update 1 record but updated %d records (ULID=%s)", n, u)
	}
	// Return whether we acquired ownership
	return n == 1, nil
}

// fetchNPartials attempts to fetch up to N partial states from the partial state table t that are not marked as being processed. The selected partial states will be marked as being processed and returned. A parallel slice of corresponding ULIDs will also be returned. If an error occurred, then the data successfully fetched so far will be returned, along with the error.
func fetchNPartials(t *keyvalue.Table, N int64) ([]*partial.State, []ulid.ULID, error) {
	// Select at most N+fetchDelta available states
	selector := record.Record{
		"beingProcessed": false,
	}
	template := record.Record{
		"encodedState": []byte{},
		"hasClosed":    false,
		"ulid":         "",
	}
	itr, err := t.SelectLimit(context.Background(), template, selector, nil, N+fetchDelta)
	if err != nil {
		return nil, nil, err
	}
	// Create the parallel slices of states and ULIDs
	S := make([]*partial.State, 0, N)
	ulids := make([]ulid.ULID, 0, N)
	// Start working through the records
	now := time.Now()
	r := record.Record{}
	for N > 0 && itr.Next() {
		// Scan the next record
		if err = itr.Scan(r); err != nil {
			break
		}
		// Recover the ULID and partial state
		var u ulid.ULID
		if u, err = ulid.FromString(r["ulid"].(string)); err != nil {
			break
		}
		var s *partial.State
		if s, err = partial.FromRecord(r); err != nil {
			break
		}
		// Try to mark this partial state as being processed
		var ok bool
		if ok, err = markPartial(t, u, now); err != nil {
			break
		} else if ok {
			// We now own this partial state
			S = append(S, s)
			ulids = append(ulids, u)
			N--
		}
	}
	// Close the iterator, checking for an error
	if e := itr.Close(); err == nil {
		if err = e; err == nil {
			err = itr.Err()
		}
	}
	// Return what we got, along with any error
	return S, ulids, err
}

// fetch attempts to fetch at most N partial states from the partial state table t. The fetched partial states, along with their ULIDs, are returned as parallel slices. If an error occurred, then the data successfully fetched so far will be returned, along with the error.
func fetch(t *keyvalue.Table, N int, lg log.Interface) (S []*partial.State, ulids []ulid.ULID, err error) {
	// Sanity check
	if N < 0 {
		N = 0
	}
	// Provide some logging feedback
	lg.Printf("Fetching partial states...")
	now := time.Now()
	defer func() {
		if err != nil {
			lg.Printf("Error whilst fetching partial states: %s", err)
		}
		lg.Printf("Fetched %d partial states in %s", len(S), time.Since(now))
	}()
	// Create the parallel slices of states and ULIDs
	S = make([]*partial.State, 0, N)
	ulids = make([]ulid.ULID, 0, N)
	// Attempt to populate the parallel slices
	ok := N > len(S)
	for ok {
		// Attempt to top up the slices
		newS, newUlids, e := fetchNPartials(t, int64(N-len(S)))
		S = append(S, newS...)
		ulids = append(ulids, newUlids...)
		// Move on
		ok = len(newS) != 0 && N > len(S) && e == nil
		err = e
	}
	return
}

// deleteULIDs deletes the records in t with the given ULIDs. This does no locking, so it should only be called with ULIDs of partial states that the calling process owns.
func deleteULIDs(t *keyvalue.Table, ulids []ulid.ULID) error {
	// Create the selector
	selector := record.Record{
		"ulid":           "",
		"beingProcessed": true,
	}
	// Delete the ULIDs
	for _, u := range ulids {
		selector["ulid"] = u.String()
		if n, err := t.Delete(context.Background(), selector); err != nil {
			return err
		} else if n != 1 {
			// This is a bad sign -- we should have deleted exactly one entry. The entire computation is now suspicious; you should probably restart the computation from the beginning.
			return fmt.Errorf("expected to delete 1 record but deleted %d records (ULID=%s)", n, u)
		}
	}
	return nil
}

// resetULIDs marks the records in t with the given ULIDs as not being processed. This does no locking, so it should only be called with ULIDs of partial states that the calling process owns.
func resetULIDs(t *keyvalue.Table, ulids []ulid.ULID) error {
	// Create the selector and update records
	selector := record.Record{
		"ulid":           "",
		"beingProcessed": true,
	}
	update := record.Record{
		"beingProcessed": false,
		"timestamp":      0,
	}
	// Mark them as not being processed
	for _, u := range ulids {
		selector["ulid"] = u.String()
		if n, err := t.Update(context.Background(), update, selector); err != nil {
			return err
		} else if n != 1 {
			// This is a bad sign -- we should have updated exactly one entry. The entire computation is now suspicious; you should probably restart the computation from the beginning.
			return fmt.Errorf("expected to update 1 record but updated %d records (ULID=%s)", n, u)
		}
	}
	return nil
}

// worker will read partial states from the cache c, and feed them down the channel destC. If the cache is empty, the function populate will be called in an attempt to populate c. The worker will exit if the cache closes its output channel, if populate returns an error not equal to errNoPartialStatesAvailable, or if the shutdownC channel is closed. The worker will close the doneC channel on exit. Logging messages will be logged to lg. Intended to be run in its own go-routine.
func worker(c *cache.Finite, populate func() error, destC chan<- *partial.State, shutdownC <-chan struct{}, doneC chan<- struct{}, lg log.Interface) {
	// Loop until not ok
	ok := true
	for ok {
		var x interface{}
		select {
		case x, ok = <-c.C:
		case _, ok = <-shutdownC:
		default:
			// If we're here then the cache was empty; wait for a while and try to populate it
			pollTimer := time.NewTimer(pollInterval)
			select {
			case x, ok = <-c.C:
				// we read a state, so continue
			case _, ok = <-shutdownC:
				// we're shutting down, so continue
			case <-pollTimer.C:
				if err := populate(); err == errNoPartialStatesAvailable {
					lg.Printf("No partial states available")
				} else if err == errCalculationFinished {
					lg.Printf("Calculation has finished")
					ok = false
				} else if err != nil {
					lg.Printf("Error populating partial state cache: %s", err)
					ok = false
				}
			}
		}
		// If we got a partial state, forward it to the user
		if x != nil {
			select {
			case destC <- x.(*partial.State):
			case _, ok = <-shutdownC:
				// Return the partial state to the cache
				if err := c.Add(x); err != nil {
					lg.Printf("Error returning partial state to cache: %s", err)
				}
			}
		}
	}
	// Close the channels and return
	lg.Printf("Partial state cache worker exiting")
	close(destC)
	close(doneC)
}

/////////////////////////////////////////////////////////////////////////
// Partial functions
/////////////////////////////////////////////////////////////////////////

// MarkAsFailed places the cache in an error state. Upon closing the cache via Close, any cached data will be dropped, and the ULIDs used to populate the cache will be marked as unprocessed.
func (c *Partial) MarkAsFailed() {
	if c != nil {
		c.m.RLock()
		defer c.m.RUnlock()
		if !c.isClosed {
			c.c.SetError(errMarkedAsFailed)
		}
	}
	return
}

// Close closes the cache. Any cached data will be flushed, and the ULIDs used to populate the cache will be deleted from the partial state table. If flushing fails, or if the cache has been placed in an error state via MarkAsFailed, then an attempt will be made to mark the ULIDs as unprocessed.
func (c *Partial) Close() (err error) {
	if c != nil {
		// Grab a lock on the metadata
		c.m.Lock()
		defer c.m.Unlock()
		if !c.isClosed {
			// Mark us as closed
			lg := c.Log()
			lg.Printf("Closing...")
			c.isClosed = true
			// Ask the background worker to exit
			close(c.shutdownC)
			<-c.doneC
			// Close the underlying cache
			c.closeErr = c.c.Close()
			// Note the ULIDs
			c.ulidm.Lock()
			ulids := c.ulids
			c.ulids = nil
			c.ulidm.Unlock()
			// Update the ULID data
			if c.closeErr == nil {
				if e := deleteULIDs(c.t, ulids); e != nil {
					c.closeErr = fmt.Errorf("error deleting processed ULIDs: %w", e)
				}
			} else {
				if c.closeErr == errMarkedAsFailed {
					c.closeErr = nil
				}
				if e := resetULIDs(c.t, ulids); e != nil {
					if c.closeErr == nil {
						c.closeErr = fmt.Errorf("error marking ULIDs as unprocessed: %w", e)
					} else {
						lg.Printf("Error marking ULIDs as unprocessed: %s", e)
					}
				}
			}
			// Close the table
			if tblErr := c.t.Close(); c.closeErr == nil {
				c.closeErr = tblErr
			}
			// Log any errors
			if c.closeErr != nil {
				lg.Printf("Closed with error: %s", c.closeErr)
			} else {
				lg.Printf("Closed")
			}
		}
		err = c.closeErr
	}
	return
}

// populate attempts to populate the cache with at most maxFetchSize partial states from the partial state table. The error errCalculationFinished indicates that the partial state table is empty, and hence that the computation has finished; the error errNoPartialStatesAvailable indicates that all partial states in the table are being handled, and the caller should wait and retry. Only intended to be called by the background worker.
func (c *Partial) populate() error {
	// Grab some states from the partial state table
	S, ulids, err := fetch(c.t, maxFetchSize, c.Log())
	if len(S) == 0 {
		if err != nil {
			return err
		} else if n, err := c.t.Count(context.Background(), nil); err != nil {
			return err
		} else if n == 0 {
			return errCalculationFinished
		}
		// Return that all the partial states are being processed -- this isn't strictly honest, but it is good enough
		return errNoPartialStatesAvailable
	}
	// Add the partial states to the cache
	for _, s := range S {
		if err := c.c.Add(s); err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
	}
	// Record the ULIDs before returning
	c.ulidm.Lock()
	defer c.ulidm.Unlock()
	c.ulids = append(c.ulids, ulids...)
	return nil
}

// Add places x onto the cache.
func (c *Partial) Add(x *partial.State) error {
	// Sanity check
	if c == nil {
		return errors.New("cannot add object to uninitialised cache")
	}
	// Are we closed?
	c.m.RLock()
	defer c.m.RUnlock()
	if c.isClosed {
		return errors.New("cannot add object to closed cache")
	}
	// Add the object to the cache
	var err error
	if x != nil {
		err = c.c.Add(x)
	}
	return err
}

// NewPartial returns a new cache for partial state data. The maximum cache size before flushing is maxSize, and the data will be written to the given table.
func NewPartial(maxSize int, t *keyvalue.Table) *Partial {
	// Create the communication channels
	destC := make(chan *partial.State)
	shutdownC := make(chan struct{})
	doneC := make(chan struct{})
	// Create the partial cache
	c := &Partial{
		Base:      newBase(maxSize, t, partialIterator),
		C:         destC,
		shutdownC: shutdownC,
		doneC:     doneC,
	}
	// Set the background worker running
	go worker(c.c, c.populate, destC, shutdownC, doneC, c.Log())
	return c
}

// NewULID returns a new ULID.
func NewULID() string {
	u, err := ulid.New()
	for err != nil {
		// wait for enough entropy
		time.Sleep(100 * time.Nanosecond)
		u, err = ulid.New()
	}
	return u.String()
}
