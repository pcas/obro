// Base provides base functions for maintaining a cache.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcastools/cache"
	"bitbucket.org/pcastools/log"
	"context"
	"fmt"
	"sync"
)

// CallbackFunc is a function that will be called after the cache has been successfully flushed to the table. If the callback function returns an error then the cache will be placed in an error state.
type CallbackFunc func(lg log.Interface) error

// iteratorFunc takes a slice of objects and returns a record iterator.
type iteratorFunc func(S []interface{}) record.Iterator

// Base is the base structure for implementing a cache.
type Base struct {
	log.BasicLogable
	c        *cache.Finite   // The underlying cache
	itrf     iteratorFunc    // The iterator function
	t        *keyvalue.Table // The destination table
	flushm   sync.Mutex      // Controls access to the flush callback
	callback CallbackFunc    // The flush callback (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// execCallback executes the given callback function. This is wrapped in a recover, with any panics returned as an error.
func execCallback(f CallbackFunc, lg log.Interface) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("panic in callback function: %w", e)
		}
	}()
	err = f(lg)
	return
}

/////////////////////////////////////////////////////////////////////////
// Base functions
/////////////////////////////////////////////////////////////////////////

// flushFunc is the flush function for this cache.
func (c *Base) flushFunc(S []interface{}) (err error) {
	// Is there anything to do?
	if len(S) == 0 {
		return
	}
	// Flush the cache to the database
	lg := c.Log()
	lg.Printf("Flushing cache (size %d)...", len(S))
	if c.t.Insert(context.Background(), c.itrf(S)); err != nil {
		lg.Printf("Flush failed with error: %s", err)
	} else {
		lg.Printf("Flush successful")
		// Call the flush callback function (if any)
		c.flushm.Lock()
		defer c.flushm.Unlock()
		if c.callback != nil {
			if err = execCallback(c.callback, lg); err != nil {
				lg.Printf("Flush callback function failed with error: %s", err)
			}
		}
	}
	return
}

// AddFlushCallback appends the function f to the list of functions that will be called on successful flush of the cache. Note that the callback will not be called if the flush fails. Note also that the callback function will not be called concurrently (within a cache -- if you share the same callback function across multiple caches then, of course, it might be called concurrently).
func (c *Base) AddFlushCallback(f CallbackFunc) {
	if c != nil && f != nil {
		// Grab a lock on the callback
		c.flushm.Lock()
		defer c.flushm.Unlock()
		// Set the callback
		if g := c.callback; g == nil {
			c.callback = f
		} else {
			c.callback = func(lg log.Interface) error {
				if err := g(lg); err != nil {
					return err
				}
				return f(lg)
			}
		}
	}
}

// Len returns the number of objects currently in the cache.
func (c *Base) Len() int {
	if c == nil {
		return 0
	}
	return c.c.Len()
}

// newBase returns a new base cache. The maximum cache size before flushing is maxSize. The data will be written to the given table using the given iterator function.
func newBase(maxSize int, t *keyvalue.Table, itrf iteratorFunc) *Base {
	c := &Base{
		t:    t,
		itrf: itrf,
	}
	c.c = cache.NewFinite(maxSize, c.flushFunc)
	return c
}
