// Completed provides a cache for completed polytopes.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package cache

import (
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/gobutil"
	"errors"
	"fmt"
	"sync"
)

// Completed is a cache for storing completed polytopes.
type Completed struct {
	*Base
	m        sync.RWMutex // Controls access to the following
	isClosed bool         // Have we closed?
	closeErr error        // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// completedIterator returns an iterator for keyvalue.Records built from the slice S of completed polytopes.
func completedIterator(S []interface{}) record.Iterator {
	return record.SliceIteratorWithConversionFunc(S, func(x interface{}) (record.Record, error) {
		// Gob-encode the entry
		enc := gobutil.NewEncoder()
		if err := enc.Encode(x); err != nil {
			gobutil.ReuseEncoder(enc)
			return nil, fmt.Errorf("encoding failed: %w", err)
		}
		b := enc.Bytes()
		gobutil.ReuseEncoder(enc)
		// Return the record
		return record.Record{"vertices": b}, nil
	})
}

/////////////////////////////////////////////////////////////////////////
// Completed functions
/////////////////////////////////////////////////////////////////////////

// Close closes the cache. Any cached data will be flushed.
func (c *Completed) Close() (err error) {
	if c != nil {
		// Lock the mutex
		c.m.Lock()
		defer c.m.Unlock()
		if !c.isClosed {
			// Grab the logger
			lg := c.Log()
			// Mark us as closed
			lg.Printf("Closing...")
			c.isClosed = true
			// Close the underlying cache and the connection to the table
			c.closeErr = c.c.Close()
			if e := c.t.Close(); c.closeErr == nil && e != nil {
				c.closeErr = fmt.Errorf("error closing table: %w", e)
			}
			// Log any errors
			if c.closeErr != nil {
				lg.Printf("Closed with error: %s", c.closeErr)
			} else {
				lg.Printf("Closed")
			}
		}
		err = c.closeErr
	}
	return
}

// Add places x onto the cache.
func (c *Completed) Add(x []*integervector.Element) error {
	// Sanity check
	if c == nil {
		return errors.New("cannot add object to uninitialised cache")
	}
	// Grab a read lock on the metadata
	c.m.RLock()
	defer c.m.RUnlock()
	// Are we closed?
	if c.isClosed {
		return errors.New("cannot add object to closed cache")
	}
	// Add the object to the cache
	var err error
	if x != nil && len(x) != 0 {
		err = c.c.Add(x)
	}
	return err
}

// NewCompleted returns a new cache for completed smooth polytopes. The maximum cache size before flushing is maxSize, and the data will be written to the given table.
func NewCompleted(maxSize int, t *keyvalue.Table) *Completed {
	return &Completed{
		Base: newBase(maxSize, t, completedIterator),
	}
}
