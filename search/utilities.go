// Utilities contains utility functions used by the Obro code.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package search

/////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////

// isDefined returns true if m[k] is defined, and false otherwise
func isDefined(m map[int]int, k int) bool {
	_, ok := m[k]
	return ok
}
