// Worker contains the worker for the Obro classification

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package search

import (
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcastools/log"
	"time"
)

// Worker is the background worker that reads partial states from the channel input and for each partial state:
// - sees whether it completes to a new Fano polytope, if outputting it to the channel completedOutput;
// - attempts to grow it by adding a single extra vertex in all possible ways, passing the results to the channel partialOutput.
// The worker exits when the done channel is closed.  On exit, it closes the channels completedOutput and partialOutput.
func Worker(input <-chan *partial.State, completedOutput chan<- interface{}, partialOutput chan<- interface{}, done <-chan struct{}, lg log.Interface) {
	cCount := 0
	pCount := 0
	var workingTime time.Duration
	// Loop until asked to exit
	lg.Printf("Worker started")
	for {
		select {
		case <-done:
			// We should shut down
			lg.Printf("Worker stopping.  Saw %v partial polytopes.  %v of them closed.  Productive work time %v", pCount, cCount, workingTime)
			close(partialOutput)
			close(completedOutput)
			return
		case s := <-input:
			t := time.Now()
			pCount++
			// First we see if this closes to a Fano polytope
			if !s.HasClosed && closesToFanoAndIsNew(s) {
				completedOutput <- s.Vertices
				s.HasClosed = true
				cCount++
			}
			// Now we attempt to grow this partial polytope
			growVertices(s, partialOutput, done)
			workingTime += time.Since(t)
		}
	}
}
