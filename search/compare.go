// Compare contains ordering functions for partial polytopes.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package search

import (
	"bitbucket.org/pcas/obro/matrix"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/slice"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// compare v1 and v2 lexicographically.  returns -1 if v1 is smaller, 1 if v2 is smaller, and 0 if equal
// FIX ME This doesn't look like lex to me (viz. the total deg comparison), but neither is it grlex or grevlex.
func comparePoints(v1 *integervector.Element, v2 *integervector.Element) int {
	h1, h2 := v1.TotalDegree(), v2.TotalDegree()
	if h1.IsGreaterThan(h2) {
		return -1
	} else if h1.IsLessThan(h2) {
		return 1
	}
	d := v1.Dimension()
	for i := 0; i < d; i++ {
		c1, c2 := v1.EntryOrPanic(i), v2.EntryOrPanic(i)
		if c1.IsLessThan(c2) {
			return -1
		} else if c1.IsGreaterThan(c2) {
			return 1
		}
	}
	return 0
}

// swapColumns swaps the i-th and j-th entries of the N-th, (N+1)-st, ... vectors in vecs
func swapColumnsFrom(vecs []*integervector.Element, i int, j int, N int) []*integervector.Element {
	result := make([]*integervector.Element, 0, len(vecs))
	for k, v := range vecs {
		var u *integervector.Element
		if k < N {
			u = v
		} else {
			sl := v.ToIntegerSlice()
			sl[i], sl[j] = sl[j], sl[i]
			u = matrix.VectorFromIntegerSlice(v.Parent().(*integervector.Parent), sl)
		}
		result = append(result, u)
	}
	return result
}

// equalColumnsUpTo returns true if for each v in the first N entries in vecs, the i-th and j-th entries of v are equal, and false otherwise
func equalColumnsUpTo(vecs []*integervector.Element, i int, j int, N int) bool {
	for _, v := range vecs[:N] {
		if !v.EntryOrPanic(i).IsEqualTo(v.EntryOrPanic(j)) {
			return false
		}
	}
	return true
}

// minimizeRow swaps columns in a matrix, represented by a sequence of row vectors rows, so as to lex-minimize the N-th row whilst fixing rows 0, 1, ..., N-1
func minimizeRow(rows []*integervector.Element, N int) []*integervector.Element {
	newRows := rows
	dimension := rows[0].Dimension()
	for col1 := 0; col1 < dimension; col1++ {
		// find a column to swap with so as to obtain a smaller row vector
		col2 := col1
		for j := col1 + 1; j < dimension; j++ {
			if newRows[N].EntryOrPanic(j).IsLessThan(newRows[N].EntryOrPanic(col2)) &&
				equalColumnsUpTo(newRows, col1, j, N) {
				col2 = j
			}
		}
		if col2 != col1 {
			newRows = swapColumnsFrom(newRows, col1, col2, N)
		}
	}
	return newRows
}

// minimizeMatrixAsRowsAndCompare minimizes a matrix, given as a slice of rows (which are integer vectors), and compares to current.Vertices
func minimizeMatrixAsRowsAndCompare(current *partial.State, rows []*integervector.Element, positions []int, trueIfEqual bool) int {
	_, _, result := minimizeBelowAndCompare(current, rows, 0, positions, trueIfEqual)
	return result
}

func minimizeBelowAndCompare(current *partial.State, rows []*integervector.Element, currentRow int, positions []int, trueIfEqual bool) ([]*integervector.Element, []int, int) {
	size := len(rows)
	// if we are past the bottom row, return 0
	if currentRow == size {
		return rows, positions, 0
	}
	// snapshot of positions
	snapshot := make([]int, len(positions))
	copy(snapshot, positions)
	// we will recursively modify the rows and positions, so copy them
	newRows := make([]*integervector.Element, size)
	copy(newRows, rows)
	newPositions := slice.CopyInt(positions)
	// try every vector in the current row
	for i := currentRow; i < size; i++ {
		// is it necessary to permute some rows?
		if snapshot[i] != newPositions[currentRow] {
			// yes it is.  Put the correct vector in the current row
			for j := currentRow + 1; j < size; j++ {
				if snapshot[i] == newPositions[j] {
					// swap rows
					newRows[currentRow], newRows[j] = newRows[j], newRows[currentRow]
					newPositions[currentRow], newPositions[j] = newPositions[j], newPositions[currentRow]
					break
				}
			}
		}
		// Minimize the current row, fixing the rows above
		newRows = minimizeRow(newRows, currentRow)
		// compare the current row with the same row in current.Vertices
		c := comparePoints(current.Vertices[current.Dimension+currentRow], newRows[currentRow])
		switch c {
		case 1:
			// the representation obtained is smaller, so return 1
			return newRows, newPositions, 1
		case 0:
			// the representations are equal up to now, so consider the rest
			var remain int
			newRows, newPositions, remain = minimizeBelowAndCompare(current, newRows, currentRow+1, newPositions, trueIfEqual)
			// if the rest is larger, return 1
			if remain == 1 {
				return newRows, newPositions, 1
			}
			// if the representations are equal, return true if we know that the vertices are in the minimal representation (i.e. if trueIfEqual is true)
			if remain == 0 && trueIfEqual {
				return newRows, newPositions, 0
			}
		default:
			// the representation obtained is larger, so try another
		}
	}
	return newRows, newPositions, -1
}

/////////////////////////////////////////////////////////////////////////
// Main comparison function
/////////////////////////////////////////////////////////////////////////

// FIX ME think and write docstring "Compare two special embeddings.  (otherVertices are not sorted by order)"
func compareRepresentation(current *partial.State, otherVertices []*integervector.Element, trueIfEqual bool) int {
	positions := make([]int, 0, len(otherVertices))
	reducedVertices := make([]*integervector.Element, 0, len(otherVertices))
	for i, v := range otherVertices {
		if v.TotalDegree().IsLessThan(one) {
			positions = append(positions, i)
			reducedVertices = append(reducedVertices, v)
		}
	}
	// Minimize reducedVertices from the top row
	return minimizeMatrixAsRowsAndCompare(current, reducedVertices, positions, trueIfEqual)
}
