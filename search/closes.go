// Closes contains the functions which determine whether a partial polytope closes to a smooth Fano polytope and, if so, whether it is new (i.e. in the correct position in Obro's order)

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package search

import (
	"bitbucket.org/pcas/obro/matrix"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/slice"
)

/////////////////////////////////////////////////////////////////////////
// local functions
/////////////////////////////////////////////////////////////////////////

// specialFaceInFacet returns true if specialFace is contained in facet, and false otherwise
func specialFaceInFacet(specialFace []int, facet []int) bool {
	for _, vidx := range specialFace {
		if slice.IndexInt(facet, vidx) == -1 {
			return false
		}
	}
	return true
}

// FIX ME think and write docstring
// Obro's comment is: returns true if ord(conv(vertices)) = vertices and false otherwise
func isNew(s *partial.State) bool {
	d := s.Dimension
	// find the special face
	vertexSum := matrix.VectorSum(s.Vertices...)
	specialFace := make([]int, 0, d)
	for i := 0; i < d; i++ {
		if vertexSum.EntryOrPanic(i).IsNegative() {
			return false
		}
		if vertexSum.EntryOrPanic(i).IsPositive() {
			specialFace = append(specialFace, i)
		}
	}
	// FIX ME THINK "try every special embedding to see if ord(convV) = V"
	for facetID := 1; facetID < len(s.Facets); facetID++ {
		if specialFaceInFacet(specialFace, s.Facets[facetID]) {
			newRepresentation := make([]*integervector.Element, 0, len(s.Vertices))
			for _, v := range s.Vertices {
				newRepresentation = append(newRepresentation, matrix.VectorMultiplyMatrix(v, s.Basechanges[facetID]))
			}
			if compareRepresentation(s, newRepresentation, true) == 1 {
				return false
			}
		}
	}
	// there is no strictly smaller special embedding
	return true
}

// closeToFano tries to add facets to the partial polytope represented by the state s, so as to produce a smooth Fano polytope.  It returns true on success and false otherwise.  Note that this modifies the state s.
func closeToFano(s *partial.State) bool {
	d := s.Dimension
	// for every facet, try to add neighbouring facets
	for f := 0; f < len(s.Facets); f++ {
		for j := 0; j < d; j++ {
			if !isDefined(s.NeighbouringFacets[f], j) {
				// do we know what the facet which borders f opposite to vertex j [THINK: numbered 0..d-1, so probably numbered in f] is
				if !addNeighbouringFacet(s, f, j) {
					return false
				}
			}
		}
	}
	// At this point every facet has neighbouring facets
	return true
}

/////////////////////////////////////////////////////////////////////////
// main function
/////////////////////////////////////////////////////////////////////////

// closesToFanoAndIsNew returns true if the partial polytope represented by the state s closes to a smooth Fano polytope and is in the correct position in Obro's order (that is, is new).  It returns false otherwise.  This function does not modify the state s.
func closesToFanoAndIsNew(s *partial.State) bool {
	// we make a copy, so that we can safely modify that copy
	s = s.Copy()
	result := closeToFano(s) && isNew(s)
	s.Delete()
	return result
}
