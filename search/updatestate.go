// Updatestate contains functions which modify the state representing a partial polytope.  They add facets and associated data, leaving the vertices fixed.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package search

import (
	"bitbucket.org/pcas/obro/matrix"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcasmath/integer"
	"bitbucket.org/pcasmath/integervector"
	"bitbucket.org/pcastools/fatal"
	"bitbucket.org/pcastools/slice"
)

// ridgeInFacet returns true, vidx if the specified ridge (which is a slice of vertex ids) is not in the facet F with index fidx, where vidx is the index of the vertex in F that is not in ridge, and false, -1 if ridge is not in F.
func ridgeInFacet(s *partial.State, fidx int, ridge []int) (bool, int) {
	vidx := -1
	// THINK: If we can arrange our data structures so that ridge is sorted,
	// this can be made much more efficient. (And I think ridge *is* sorted?)
	for i, k := range s.Facets[fidx] {
		if slice.IndexInt(ridge, k) == -1 {
			// the i-th vertex of the facet is not in the ridge
			if vidx != -1 {
				return false, -1
			}
			vidx = i
		}
	}
	return true, vidx
}

// closeRidges attempts to close all ridges (that is, all codimension 2 faces) of the partial polytope represented by s.  It returns true on success, false if a contradiction was encountered.  Note that this will modify the state s
func closeRidges(s *partial.State) bool {
	d := s.Dimension
	lastFidx := len(s.Facets) - 1
	lastFacet := s.Facets[lastFidx]
	for i := 0; i < d; i++ {
		// create a ridge: the last facet with its i-th vertex omitted
		ridge := make([]int, 0, d-1)
		ridge = append(ridge, lastFacet[:i]...)
		ridge = append(ridge, lastFacet[i+1:]...)
		// find a facet containing this ridge that is *not* the most-recently added facet
		for fidx := 0; fidx < lastFidx; fidx++ {
			if ok, vidx := ridgeInFacet(s, fidx, ridge); ok {
				// if the neighbouring facet is not unique, return false
				if isDefined(s.NeighbouringFacets[fidx], vidx) {
					return false
				}
				s.NeighbouringFacets[fidx][vidx] = lastFidx
				s.NeighbouringFacets[lastFidx][i] = fidx
				break
			}
		}
	}
	return true
}

// makeBasisChangeMatrix returns a matrix M' defined as follows.  If M is such that v_i*M = e_i, where v_1,...,v_n are vertices of a facet (as row vectors) and e_1,...,e_n are the standard basis vectors (as row vectors) then v'_i M' = e_i where v'_i = v_i if i ne k and v'_k = v.
func makeBasisChangeMatrix(M matrix.Matrix, v *integervector.Element, k int) matrix.Matrix {
	rel := matrix.VectorMultiplyMatrix(v, M).ToIntegerSlice()
	rel[k] = minustwo
	res := M.Columns() // Starts off as the columns of M and we then correct it
	kcol := M.Column(k)
	for j, r := range rel {
		u, err := integervector.ScalarMultiplyByIntegerThenAdd(r, kcol, res[j])
		if err != nil {
			panic(fatal.ImpossibleError(err)) // This should never happen
		}
		res[j] = u
	}
	return matrix.Matrix(res)
}

// makeNewFacet tries to adds a new facet to the partial polytope represented by the state s.  This new facet meets oldFacet along the ridge opposite to vertex oppositeNumber [FIX ME: number in the facet, I believe] and has other vertex neighbouringVertex.  It returns true if the modification is successful, and false if it resulted in a contradiction.  Note that this will modify the state s.
func makeNewFacet(s *partial.State, oldFacet int, neighbouringVertex int, oppositeNumber int) bool {
	// make the new facet and add it
	newFacet := make([]int, len(s.Facets[oldFacet]))
	copy(newFacet, s.Facets[oldFacet])
	newFacet[oppositeNumber] = neighbouringVertex
	s.Facets = append(s.Facets, newFacet)
	// make the new basechange matrix and add it
	M := makeBasisChangeMatrix(s.Basechanges[oldFacet], s.Vertices[neighbouringVertex], oppositeNumber)
	s.Basechanges = append(s.Basechanges, M)
	// make the new normal vector and add it
	s.Normals = append(s.Normals, matrix.VectorFromIntegerSlice(s.Lattice, M.RowSums()))
	// make the new neighbours
	s.NeighbouringFacets = append(s.NeighbouringFacets, make(map[int]int))
	// try to close the ridges of the new facet
	if !closeRidges(s) {
		return false
	}
	// check the new facet is compatible with each of the vertices
	newFacetIdx := len(s.Facets) - 1
	numVerts := len(s.Vertices)
	for v := 0; v < numVerts; v++ {
		if !vertexFacetCheck(s, newFacetIdx, v) {
			return false
		}
	}
	// we successfully added a facet
	return true
}

// FIX ME THINK and write docstring
// This appears to look for a cheap contradiction to throwing in a vertex. Returns true if no contradiction was found. Note that this will modify the state s.
func vertexFacetCheck(s *partial.State, facetID int, vertexID int) bool {
	// If the vertex is in the facet then there is nothing to check
	if slice.IndexInt(s.Facets[facetID], vertexID) != -1 {
		return true
	}
	// which hyperplane is the vertex in?
	hyperplane := matrix.DotProduct(s.Normals[facetID], s.Vertices[vertexID])
	// if the vertex is not strictly beneath the facet then fail
	if hyperplane.IsPositive() {
		return false
	}
	isZero := hyperplane.IsZero()
	// FIX ME
	d := s.Dimension
	for j := 0; j < d; j++ {
		if !isDefined(s.NeighbouringFacets[facetID], j) {
			// calculate the coefficient of the j-th basis vector with respect to the basis V(facet) FIX ME whatever this means
			coeff := matrix.DotProduct(s.Vertices[vertexID], s.Basechanges[facetID].Column(j))
			// add a facet if vertex is in the zero hyperplane or is a neighbouring vertex of the initial facet
			if isZero || facetID == 0 {
				if coeff.IsLessThanInt64(-1) || (coeff.IsEqualToInt64(-1) && !makeNewFacet(s, facetID, vertexID, j)) {
					return false
				}
			} else if coeff.IsLessThan(hyperplane) {
				// fail if coeff exceeds the lower bound
				return false
			}
		}
	}
	return true
}

// addNeighbouringFacet tries to add a facet to the partial polytope represented by the state s.  It returns true on success and false otherwise. The facet to add is adjacent to the facet F with index fidx, opposite the vertex of F with index vidx.  Here vidx is the index among vertices of F, not among all vertices. Note that this modifies the state s
func addNeighbouringFacet(s *partial.State, fidx int, vidx int) bool {
	// find the height and coefficient for each vertex
	n := s.Normals[fidx]
	col := s.Basechanges[fidx].Column(vidx)
	numVerts := len(s.Vertices)
	hs := make([]*integer.Element, 0, numVerts)
	cs := make([]*integer.Element, 0, numVerts)
	for _, v := range s.Vertices {
		hs = append(hs, matrix.DotProduct(v, n))
		cs = append(cs, matrix.DotProduct(v, col))
	}
	// find the neighbouring vertex
	var neighbouringVertex int
	success := false
	for i, c := range cs {
		if c.IsEqualToInt64(-1) {
			success = true
			neighbouringVertex = i
			for j := i + 1; j < numVerts; j++ {
				if cs[j].IsEqualToInt64(-1) && hs[neighbouringVertex].IsLessThan(hs[j]) {
					neighbouringVertex = j
				}
			}
			break
		}
	}
	if !success {
		return false
	}
	// if another vertex is not below the facet that we are about to add then fail
	h := hs[neighbouringVertex].Decrement()
	for i, c := range cs {
		if i != neighbouringVertex && c.IsNegative() && integer.MultiplyThenAdd(c, h, hs[i]).IsPositive() {
			return false
		}
	}
	return makeNewFacet(s, fidx, neighbouringVertex, vidx)
}
