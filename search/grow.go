// Grow is the heart of the algorithm. It takes a partial polytope and attempts to enlarge it by adding a single vertex in all possible ways.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package search

import (
	"bitbucket.org/pcas/obro/matrix"
	"bitbucket.org/pcas/obro/partial"
	"bitbucket.org/pcasmath/integer"
	"bitbucket.org/pcasmath/integervector"
	"github.com/pkg/errors"
)

// ErrCantFindVertex is returned when a new vertex cannot be found within the given bounds.
var ErrCantFindVertex = errors.New("Can not find vertex with given bounds and height")

// small integers
var zero = integer.Zero()
var one = integer.One()
var two = integer.FromInt(2)
var minusone = integer.FromInt(-1)
var minustwo = integer.FromInt(-2)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// makeLowerBound returns a lower bound for possible vertices at height h that extend the partial polytope s
func makeLowerBound(s *partial.State, h *integer.Element) *integervector.Element {
	/* why is this correct?  recall that h is <= 0 here, and also that either (A) we know the facet F_i adjacent to the standard facet along the ridge opposite the i-th standard basis vector e_i; or (B) we don't.  In case (A), neighboursInHyperplane[i] is equal to the height of the vertex v_i of F_i that is not in the standard facet; in case (B) neighboursInHyperplane[i] is zero.

				Case 1: h==0.  Then -1 <= a_i from Definition 3.5 on page 7 of Obro's paper.  Also we might have some tighter bound on a_i coming from the facet F_i if we are in case (A).
				So:

				result starts as (-1,-1,...,-1).  Then, for each i:

					if we are in case (B) then k starts as 1, remains as 1 all the way through the i loop, and we end up with the bound a_i >= -1

				        if we are in case (A) then k starts as 1-H_i.  This is equal to the height of the vertex (a_1,...,a_n) with respect to F_i by Lemma 2.2(3) in Obro's paper, and that height needs to be less than 1 [it is obviously at most 1, and it can't be 1 because if it were then we would be adding a new vertex of F' and we already know all the vertices of F'].  If H_i = 0 then we pass through the loop precisely once, obtaining a_i >= 0.  If H_i < 0 then we pass through the loop (incrementing the bound B) until the height with respect to F' is less than 1, obtaining a_i >=B.  In each case the bound is correct.

				        [ note that 2h + vertexSumHeight is never negative in this case, because h==0 and vertexSum lies in the convex hull of the special facet (= the non-negative orthant) ]

				So case 1 is correct.

				Case 2: h<0  Then h <= a_i from Definition 3.5 on page 7 of Obro's paper.  Also we might have some tighter bound on a_i coming from the facet F_i if we are in case (A).

				result starts as (h-1,...,h-1)

				note that if 2h+vertexSumHeight is negative then this is the last vertex that we can possibly add (because any other vertices will be added at height h or less, and then adding such would mean that the vertex sum would not lie in the positive orthant, contradicting the fact that <e1,...,en> is a special facet.  So in this case we certainly need result_i + vertexSum_i >=0, because we need the overall vertex sum to lie in the positive orthant.  The last case in the inner for loop adjusts result until this is the case.

		the other cases in the inner for loop ensure the following.  If we know F_i then k is equal (by Lemma 2.2(3) in Obro's paper again) to the height with respect to F_i of the vertex that we are adding.  This clearly needs to be at most 1; if we already know F_i then it must in fact be less than 1 (because otherwise we would be adding a vertex of F_i, but in fact we already know all the vertices of F_i).   If we don't know F_i then we get the same bounds but with neighboursInHyperplane[i] set to zero (because in the end this will be set equal to something, and that something is certainly <=0, and 0 gives the slackest bounds).  There is only one difference in this case: if we don't already know F_i then the height with respect to F_i of the new vertex is allowed to be 1 provided that the new vertex could actually be attached to the standard facet opposite to the i-th standard basis vector (that is, provided that a_i>=-1).

	        So case 2 is correct too
	*/
	d := s.Dimension
	// we start the lower bound as (h-1,...,h-1)
	startingSlice := make([]*integer.Element, 0, d)
	hMinusOne := h.Decrement()
	for i := 0; i < d; i++ {
		startingSlice = append(startingSlice, hMinusOne)
	}
	result, err := integervector.FromIntegerSlice(s.Lattice, startingSlice)
	if err != nil {
		panic(err)
	}
	// now improve the lower bound
	vertexSum := matrix.VectorSum(s.Vertices...)
	vertexSumHeight := vertexSum.TotalDegree()
	for i := 0; i < d; i++ {
		k := integer.MultiplyThenAdd(result.EntryOrPanic(i), s.NeighboursInHyperplane[i].Decrement(), h)
		for k.IsGreaterThan(one) ||
			(k.IsOne() &&
				result.EntryOrPanic(i).IsLessThanInt64(-1)) ||
			(k.IsOne() &&
				isDefined(s.NeighbouringFacets[0], i)) ||
			(integer.MultiplyThenAdd(two, h, vertexSumHeight).IsNegative() &&
				integer.Add(result.EntryOrPanic(i), vertexSum.EntryOrPanic(i)).IsNegative()) {
			// increment the i-th entry in the lower bound
			result = result.IncrementEntryOrPanic(i)
			// and adjust k
			k = integer.Sum(k, s.NeighboursInHyperplane[i], minusone)
		}
	}
	return result
}

// makeUpperBound returns an upper bound for possible vertices at height h that extend the partial polytope s
func makeUpperBound(s *partial.State, h *integer.Element) *integervector.Element {
	d := s.Dimension
	ub := make([]*integer.Element, 0, d)
	vertexSum := matrix.VectorSum(s.Vertices...)
	vertexSumHeight := vertexSum.TotalDegree()
	ltzero := integer.MultiplyThenAdd(two, h, vertexSumHeight).IsLessThan(zero)
	for i := 0; i < d; i++ {
		var u *integer.Element
		if ltzero && !isDefined(s.NeighbouringFacets[0], i) {
			u = minusone
		} else {
			// I am sceptical about these bounds, so I have replaced them with the bounds from Definition 3.5 in Obro's paper.
			// if h.IsZero() && !isDefined(s.NeighbouringFacets[0], i) {
			// 	u = integer.Sum(h, vertexSumHeight, vertexSum.EntryOrPanic(i).Negate(), one)
			// } else {
			// 	u = integer.Sum(h, vertexSumHeight, vertexSum.EntryOrPanic(i).Negate())
			//}
			if h.IsZero() {
				u = integer.FromInt(d - 1)
			} else {
				u = integer.Add(integer.FromInt(d), h)
			}
		}
		ub = append(ub, u)
	}
	return matrix.VectorFromIntegerSlice(s.Lattice, ub)
}

// boundsAreCompatible returns true if every entry of lowerBound is less than or equal to the corresponding element of upperBound, and false otherwise.  It will panic if upperBound and lowerBound have different dimensions.
func boundsAreCompatible(lowerBound *integervector.Element, upperBound *integervector.Element) bool {
	d := lowerBound.Dimension()
	if upperBound.Dimension() != d {
		panic("boundsAreCompatible called with arguments of differing dimension.")
	}
	for i := 0; i < d; i++ {
		if lowerBound.EntryOrPanic(i).IsGreaterThan(upperBound.EntryOrPanic(i)) {
			return false
		}
	}
	return true
}

// canAddVertex does cheap tests, which require no modification of the state s, to see if the vertex v can be used to extend the partial polytope s
func canAddVertex(s *partial.State, v *integervector.Element) bool {
	// is the new vertex beneath every known facet?
	for _, n := range s.Normals[1:len(s.Facets)] {
		if matrix.DotProduct(n, v).IsPositive() {
			return false
		}
	}
	return true
}

// addVertex tries to extend the partial polytope s using the vector v at height h.  It returns t, true on success, where t is an updated state representing the extended partial polytope; it returns nil, false if the extension was not possible.
func addVertex(s *partial.State, h *integer.Element, v *integervector.Element) (*partial.State, bool) {
	// Make a copy of s and append the vertex v
	s = s.Copy()
	s.Vertices = append(s.Vertices, v)
	s.HasClosed = false
	// We note where the search will be resumed from; namely, the smallest
	// vector larger than v
	s.ResumeFrom = v.IncrementEntryOrPanic(v.Dimension() - 1)
	// is v a neighbouring vertex to the initial facet?
	d := s.Dimension
	for i := 0; i < d; i++ {
		if v.EntryOrPanic(i).IsEqualToInt64(-1) && !isDefined(s.NeighbouringVertices, i) {
			s.NeighbouringVertices[i] = len(s.Vertices) - 1
			s.NeighboursInHyperplane[i] = h
		}
	}
	// FIX ME check "consequences of the new vertex"
	numFacets := len(s.Facets)
	for facetID := 0; facetID < numFacets; facetID++ {
		if !vertexFacetCheck(s, facetID, len(s.Vertices)-1) {
			s.Delete()
			return nil, false
		}
	}
	return s, true
}

// firstEntriesAreOK checks whether the first entries (up to position) in newVertex, which is being constructed recursively, are compatible with the lowerBound and upperBound, and whether it is possible to complete this and still get something lying in the hyperplane at height h.  Returns true if so and false otherwise.
func firstEntriesAreOK(newVertex *integervector.Element, h *integer.Element, lowerBound *integervector.Element, upperBound *integervector.Element, position int) bool {
	// we have chosen the first entries up to position
	// do they violate the bounds?
	for i := 0; i < position; i++ {
		if newVertex.EntryOrPanic(i).IsLessThan(lowerBound.EntryOrPanic(i)) || newVertex.EntryOrPanic(i).IsGreaterThan(upperBound.EntryOrPanic(i)) {
			return false
		}
	}
	// are the bounds compatible with each other
	if !boundsAreCompatible(lowerBound, upperBound) {
		return false
	}
	// is it possible to hit the hyperplane h with these first position entries fixed and staying inside the upper and lower bounds?  If so, return true, otherwise return false
	partialSum := integer.Sum(newVertex.ToIntegerSlice()[:position+1]...)
	if integer.Add(integer.Sum(lowerBound.ToIntegerSlice()[position+1:]...), partialSum).IsGreaterThan(h) {
		return false
	} else if integer.Add(integer.Sum(upperBound.ToIntegerSlice()[position+1:]...), partialSum).IsLessThan(h) {
		return false
	}
	return true
}

// nextVertex returns the next vertex greater than or equal to currentVertex that satisfies the bounds given by lowerBound and upperBound and is in the hyperplane at height h.  This is constructed recursively, and position is the index up to which we have computed the result so far.  nextVertex returns result, nil on success and nil, error if no such vertex exists.
func nextVertex(currentVertex *integervector.Element, h *integer.Element, position int, lowerBound *integervector.Element, upperBound *integervector.Element, current *partial.State) (*integervector.Element, error) {
	d := current.Dimension
	// have we constructed every entry?
	if position == d {
		if integer.GCD(currentVertex.ToIntegerSlice()...).IsOne() {
			return currentVertex, nil
		}
		return nil, ErrCantFindVertex
	}
	// find the interval [begin,end] that the entry in position can be in.
	newVertex := currentVertex
	begin := integer.MaxOfPair(lowerBound.EntryOrPanic(position), currentVertex.EntryOrPanic(position))
	cv := currentVertex.ToIntegerSlice()
	lb := lowerBound.ToIntegerSlice()
	end := integer.MinOfPair(upperBound.EntryOrPanic(position), integer.Sum(h, integer.Sum(cv[:position]...).Negate(), integer.Sum(lb[position+1:]...).Negate()))
	for coord := begin; coord.IsLessThanOrEqualTo(end); coord = coord.Increment() {
		// If the entry in position is equal to coord, how does the lower bound change?
		newLowerBound := lowerBound
		vidx, ok := current.NeighbouringVertices[position]
		if ok {
			k := integer.MultiplyThenAdd(coord, current.NeighboursInHyperplane[position].Decrement(), h)
			for i := 0; i < d; i++ {
				if i != position {
					offset := k
					if k.IsZero() {
						offset = minusone
					}
					c := integer.MultiplyThenAdd(coord.Negate(), current.Vertices[vidx].EntryOrPanic(i), offset)
					if c.IsGreaterThan(lowerBound.EntryOrPanic(i)) {
						sl := newLowerBound.ToIntegerSlice()
						sl[i] = c
						newLowerBound = matrix.VectorFromIntegerSlice(current.Lattice, sl)
					}
				}
			}
		}
		// now update the new vertex.  sl is the new vertex as a slice
		sl := newVertex.ToIntegerSlice()
		// If the entry in position is increased, then set the subsequent entries to the lower bound.
		if coord.IsGreaterThan(currentVertex.EntryOrPanic(position)) {
			for i := position + 1; i < d; i++ {
				sl[i] = newLowerBound.EntryOrPanic(i)
			}
		}
		// set the entry in position equal to coord
		sl[position] = coord
		// rewrite the slice as a vector
		newVertex = matrix.VectorFromIntegerSlice(current.Lattice, sl)
		// are the first entries OK?
		if firstEntriesAreOK(newVertex, h, newLowerBound, upperBound, position) {
			// can we choose the subsequent entries
			newVertex, err := nextVertex(newVertex, h, position+1, newLowerBound, upperBound, current)
			if err == nil {
				return newVertex, nil
			}
		}
	}
	// we cannot choose the last entries
	return nil, ErrCantFindVertex
}

// growVerticesAtHeight attempts to grow the partial polytope s by adding exactly one vertex at height h in all possible ways.  The resulting partial polytopes are fed to the channel outputChan.  The function returns true if the search should continue to lower heights, and false otherwise.  The function returns false when the done channel is closed, pushing the current state to the outputChan before returning.
func growVerticesAtHeight(s *partial.State, h *integer.Element, outputChan chan<- interface{}, done <-chan struct{}) bool {
	// make the lower and upper bounds
	lowerBound := makeLowerBound(s, h)
	upperBound := makeUpperBound(s, h)
	// the lower bound must be less than the upper bound
	// FIX ME check the maths here
	if !boundsAreCompatible(lowerBound, upperBound) {
		// there is no way to add another vertex, so stop the search
		return false
	}
	// now start or resume the search
	resumeFrom := lowerBound
	if s.ResumeFrom != nil && s.ResumeFromHeight.IsEqualTo(h) {
		resumeFrom = s.ResumeFrom
	}
	// find the next legitimate vertex in the hyperplane at height h
	// (in strictly increasing order)
	newVertex, err := nextVertex(resumeFrom, h, 0, lowerBound, upperBound, s)
	for err == nil {
		// do cheap tests that check if adding newVertex leads to a contradiction
		subsequentVertex := newVertex.IncrementEntryOrPanic(newVertex.Dimension() - 1)
		if !canAddVertex(s, newVertex) {
			newVertex, err = nextVertex(subsequentVertex, h, 0, lowerBound, upperBound, s)
			continue
		}
		// Add the vertex (and update facets, etc.)
		if newState, ok := addVertex(s, h, newVertex); ok {
			if compareRepresentation(newState, newState.Vertices, false) != 1 {
				outputChan <- newState
			} else {
				newState.Delete()
			}
		}
		// check whether we are supposed to be shutting down
		select {
		case <-done:
			// Push a copy of the current state onto the output channel
			// (we make a copy so that we can modify it)
			current := s.Copy()
			current.ResumeFromHeight = h
			current.ResumeFrom = subsequentVertex
			outputChan <- current
			// return false to stop the search
			return false
		default:
			// we continue the search
		}
		// we keep looking.  Try to find the next vertex at this height.
		// We start looking at the smallest vector that is larger than newVertex
		newVertex, err = nextVertex(subsequentVertex, h, 0, lowerBound, upperBound, s)
	}
	return true
}

/////////////////////////////////////////////////////////////////////////
// Main function
/////////////////////////////////////////////////////////////////////////

// growVertices attempts to grow the partial polytope s by adding exactly one vertex in all possible ways.  The resulting partial polytopes are fed to the channel outputChan. The function returns when the done channel is closed; we push our current state to the outputChan before returning.
func growVertices(s *partial.State, outputChan chan<- interface{}, done <-chan struct{}) {
	// we add vertices height-by-height.
	// compute the initial (that is, maximum) height
	var initialHeight *integer.Element
	switch {
	case s.ResumeFromHeight != nil:
		initialHeight = s.ResumeFromHeight
	case len(s.Vertices) > s.Dimension:
		initialHeight = s.Vertices[len(s.Vertices)-1].TotalDegree()
	default:
		initialHeight = zero
	}
	// compute the final (that is, minimum) height
	// this comes from the discussion immediately after Definition 3.1 in Obro's paper arXiv:0704.0049v1
	minusVertexSumHeight := matrix.VectorSum(s.Vertices...).TotalDegree().Negate()
	// grow the partial polytope s by adding vertices height-by-height
	for h := initialHeight; h.IsGreaterThanOrEqualTo(minusVertexSumHeight); h = h.Decrement() {
		if !growVerticesAtHeight(s, h, outputChan, done) {
			// if there are no vertices to add at height h, then there are
			// none at lower heights either
			return
		}
	}
}
